<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-org-openstreetmap-nominatim-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Osm;

/**
 * ApiOrgOpenstreetmapNominatimNameDetails class file.
 * 
 * This is a simple implementation of the
 * ApiOrgOpenstreetmapNominatimNameDetailsInterface.
 * 
 * /!\ This file was generated automatically from the json-schema.json file.
 * /!\ Do not edit by hand or the modifications will be erased.
 * @generator PhpExtended\JsonSchema\Php74ClassMetadata
 * 
 * @author Anastaszor
 */
class ApiOrgOpenstreetmapNominatimNameDetails implements ApiOrgOpenstreetmapNominatimNameDetailsInterface
{
	
	/**
	 * The canonical name.
	 * 
	 * @var string
	 */
	protected string $_name;
	
	/**
	 * The names in other languages.
	 * 
	 * @var array<string, string>
	 */
	protected array $_names = [];
	
	/**
	 * Constructor for ApiOrgOpenstreetmapNominatimNameDetails with private members.
	 * 
	 * @param string $name
	 * @param array<string, string> $names
	 */
	public function __construct(string $name, array $names)
	{
		$this->setName($name);
		$this->setNames($names);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * Sets the canonical name.
	 * 
	 * @param string $name
	 * @return ApiOrgOpenstreetmapNominatimNameDetailsInterface
	 */
	public function setName(string $name) : ApiOrgOpenstreetmapNominatimNameDetailsInterface
	{
		$this->_name = $name;
		
		return $this;
	}
	
	/**
	 * Gets the canonical name.
	 * 
	 * @return string
	 */
	public function getName() : string
	{
		return $this->_name;
	}
	
	/**
	 * Sets the names in other languages.
	 * 
	 * @param array<string, string> $names
	 * @return ApiOrgOpenstreetmapNominatimNameDetailsInterface
	 */
	public function setNames(array $names) : ApiOrgOpenstreetmapNominatimNameDetailsInterface
	{
		$this->_names = $names;
		
		return $this;
	}
	
	/**
	 * Gets the names in other languages.
	 * 
	 * @return array<string, string>
	 */
	public function getNames() : array
	{
		return $this->_names;
	}
	
}
