<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-org-openstreetmap-nominatim-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Osm;

use PhpExtended\GeoJson\GeoJsonBoundingBoxInterface;
use PhpExtended\GeoJson\GeoJsonGeometryInterface;

/**
 * ApiOrgOpenstreetmapNominatimGeocodingResult class file.
 * 
 * This is a simple implementation of the
 * ApiOrgOpenstreetmapNominatimGeocodingResultInterface.
 * 
 * /!\ This file was generated automatically from the json-schema.json file.
 * /!\ Do not edit by hand or the modifications will be erased.
 * @generator PhpExtended\JsonSchema\Php74ClassMetadata
 * 
 * @author Anastaszor
 * @SuppressWarnings("PHPMD.ExcessivePublicCount")
 * @SuppressWarnings("PHPMD.LongClassName")
 * @SuppressWarnings("PHPMD.TooManyFields")
 * @SuppressWarnings("PHPMD.TooManyPublicMethods")
 */
class ApiOrgOpenstreetmapNominatimGeocodingResult implements ApiOrgOpenstreetmapNominatimGeocodingResultInterface
{
	
	/**
	 * The full address of this object.
	 * 
	 * @var ?ApiOrgOpenstreetmapNominatimGeocodingAddressInterface
	 */
	protected ?ApiOrgOpenstreetmapNominatimGeocodingAddressInterface $_address = null;
	
	/**
	 * The bounding box that encloses the shape of this object.
	 * 
	 * @var ?GeoJsonBoundingBoxInterface
	 */
	protected ?GeoJsonBoundingBoxInterface $_boundingBox = null;
	
	/**
	 * The class of the object.
	 * 
	 * @var string
	 */
	protected string $_class;
	
	/**
	 * The full display name of the location of the object.
	 * 
	 * @var string
	 */
	protected string $_displayName;
	
	/**
	 * The importance of the object.
	 * 
	 * @var string
	 */
	protected string $_importance;
	
	/**
	 * The latitude of the object.
	 * 
	 * @var string
	 */
	protected string $_latitude;
	
	/**
	 * The licence text for using this api.
	 * 
	 * @var string
	 */
	protected string $_licence;
	
	/**
	 * The longitude of the object.
	 * 
	 * @var string
	 */
	protected string $_longitude;
	
	/**
	 * The id of the nearest osm object.
	 * 
	 * @var string
	 */
	protected string $_osmId;
	
	/**
	 * The type of the nearest osm object.
	 * 
	 * @var string
	 */
	protected string $_osmType;
	
	/**
	 * The id of the place this object is on.
	 * 
	 * @var string
	 */
	protected string $_placeId;
	
	/**
	 * The id of the place this object is ranked to.
	 * 
	 * @var string
	 */
	protected string $_placeRank;
	
	/**
	 * The svg string that shapes this object.
	 * 
	 * @var string
	 */
	protected string $_svg;
	
	/**
	 * The category of the object.
	 * 
	 * @var string
	 */
	protected string $_category;
	
	/**
	 * The type of object.
	 * 
	 * @var string
	 */
	protected string $_type;
	
	/**
	 * The type of the address.
	 * 
	 * @var ?string
	 */
	protected ?string $_addresstype = null;
	
	/**
	 * The icon for this place.
	 * 
	 * @var string
	 */
	protected string $_icon;
	
	/**
	 * The geojson polygon, in case asked for.
	 * 
	 * @var ?GeoJsonGeometryInterface
	 */
	protected ?GeoJsonGeometryInterface $_geojson = null;
	
	/**
	 * The geokml polygon, in case asked for.
	 * 
	 * @var ?string
	 */
	protected ?string $_geokml = null;
	
	/**
	 * The geotext polygon, in case asked for.
	 * 
	 * @var ?string
	 */
	protected ?string $_geotext = null;
	
	/**
	 * The name of the locality.
	 * 
	 * @var ?string
	 */
	protected ?string $_name = null;
	
	/**
	 * The name details, if requested.
	 * 
	 * @var ?ApiOrgOpenstreetmapNominatimNameDetailsInterface
	 */
	protected ?ApiOrgOpenstreetmapNominatimNameDetailsInterface $_nameDetails = null;
	
	/**
	 * Constructor for ApiOrgOpenstreetmapNominatimGeocodingResult with private members.
	 * 
	 * @param string $class
	 * @param string $displayName
	 * @param string $importance
	 * @param string $latitude
	 * @param string $licence
	 * @param string $longitude
	 * @param string $osmId
	 * @param string $osmType
	 * @param string $placeId
	 * @param string $placeRank
	 * @param string $svg
	 * @param string $category
	 * @param string $type
	 * @param string $icon
	 * @SuppressWarnings("PHPMD.ExcessiveParameterList")
	 */
	public function __construct(string $class, string $displayName, string $importance, string $latitude, string $licence, string $longitude, string $osmId, string $osmType, string $placeId, string $placeRank, string $svg, string $category, string $type, string $icon)
	{
		$this->setClass($class);
		$this->setDisplayName($displayName);
		$this->setImportance($importance);
		$this->setLatitude($latitude);
		$this->setLicence($licence);
		$this->setLongitude($longitude);
		$this->setOsmId($osmId);
		$this->setOsmType($osmType);
		$this->setPlaceId($placeId);
		$this->setPlaceRank($placeRank);
		$this->setSvg($svg);
		$this->setCategory($category);
		$this->setType($type);
		$this->setIcon($icon);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * Sets the full address of this object.
	 * 
	 * @param ?ApiOrgOpenstreetmapNominatimGeocodingAddressInterface $address
	 * @return ApiOrgOpenstreetmapNominatimGeocodingResultInterface
	 */
	public function setAddress(?ApiOrgOpenstreetmapNominatimGeocodingAddressInterface $address) : ApiOrgOpenstreetmapNominatimGeocodingResultInterface
	{
		$this->_address = $address;
		
		return $this;
	}
	
	/**
	 * Gets the full address of this object.
	 * 
	 * @return ?ApiOrgOpenstreetmapNominatimGeocodingAddressInterface
	 */
	public function getAddress() : ?ApiOrgOpenstreetmapNominatimGeocodingAddressInterface
	{
		return $this->_address;
	}
	
	/**
	 * Sets the bounding box that encloses the shape of this object.
	 * 
	 * @param ?GeoJsonBoundingBoxInterface $boundingBox
	 * @return ApiOrgOpenstreetmapNominatimGeocodingResultInterface
	 */
	public function setBoundingBox(?GeoJsonBoundingBoxInterface $boundingBox) : ApiOrgOpenstreetmapNominatimGeocodingResultInterface
	{
		$this->_boundingBox = $boundingBox;
		
		return $this;
	}
	
	/**
	 * Gets the bounding box that encloses the shape of this object.
	 * 
	 * @return ?GeoJsonBoundingBoxInterface
	 */
	public function getBoundingBox() : ?GeoJsonBoundingBoxInterface
	{
		return $this->_boundingBox;
	}
	
	/**
	 * Sets the class of the object.
	 * 
	 * @param string $class
	 * @return ApiOrgOpenstreetmapNominatimGeocodingResultInterface
	 */
	public function setClass(string $class) : ApiOrgOpenstreetmapNominatimGeocodingResultInterface
	{
		$this->_class = $class;
		
		return $this;
	}
	
	/**
	 * Gets the class of the object.
	 * 
	 * @return string
	 */
	public function getClass() : string
	{
		return $this->_class;
	}
	
	/**
	 * Sets the full display name of the location of the object.
	 * 
	 * @param string $displayName
	 * @return ApiOrgOpenstreetmapNominatimGeocodingResultInterface
	 */
	public function setDisplayName(string $displayName) : ApiOrgOpenstreetmapNominatimGeocodingResultInterface
	{
		$this->_displayName = $displayName;
		
		return $this;
	}
	
	/**
	 * Gets the full display name of the location of the object.
	 * 
	 * @return string
	 */
	public function getDisplayName() : string
	{
		return $this->_displayName;
	}
	
	/**
	 * Sets the importance of the object.
	 * 
	 * @param string $importance
	 * @return ApiOrgOpenstreetmapNominatimGeocodingResultInterface
	 */
	public function setImportance(string $importance) : ApiOrgOpenstreetmapNominatimGeocodingResultInterface
	{
		$this->_importance = $importance;
		
		return $this;
	}
	
	/**
	 * Gets the importance of the object.
	 * 
	 * @return string
	 */
	public function getImportance() : string
	{
		return $this->_importance;
	}
	
	/**
	 * Sets the latitude of the object.
	 * 
	 * @param string $latitude
	 * @return ApiOrgOpenstreetmapNominatimGeocodingResultInterface
	 */
	public function setLatitude(string $latitude) : ApiOrgOpenstreetmapNominatimGeocodingResultInterface
	{
		$this->_latitude = $latitude;
		
		return $this;
	}
	
	/**
	 * Gets the latitude of the object.
	 * 
	 * @return string
	 */
	public function getLatitude() : string
	{
		return $this->_latitude;
	}
	
	/**
	 * Sets the licence text for using this api.
	 * 
	 * @param string $licence
	 * @return ApiOrgOpenstreetmapNominatimGeocodingResultInterface
	 */
	public function setLicence(string $licence) : ApiOrgOpenstreetmapNominatimGeocodingResultInterface
	{
		$this->_licence = $licence;
		
		return $this;
	}
	
	/**
	 * Gets the licence text for using this api.
	 * 
	 * @return string
	 */
	public function getLicence() : string
	{
		return $this->_licence;
	}
	
	/**
	 * Sets the longitude of the object.
	 * 
	 * @param string $longitude
	 * @return ApiOrgOpenstreetmapNominatimGeocodingResultInterface
	 */
	public function setLongitude(string $longitude) : ApiOrgOpenstreetmapNominatimGeocodingResultInterface
	{
		$this->_longitude = $longitude;
		
		return $this;
	}
	
	/**
	 * Gets the longitude of the object.
	 * 
	 * @return string
	 */
	public function getLongitude() : string
	{
		return $this->_longitude;
	}
	
	/**
	 * Sets the id of the nearest osm object.
	 * 
	 * @param string $osmId
	 * @return ApiOrgOpenstreetmapNominatimGeocodingResultInterface
	 */
	public function setOsmId(string $osmId) : ApiOrgOpenstreetmapNominatimGeocodingResultInterface
	{
		$this->_osmId = $osmId;
		
		return $this;
	}
	
	/**
	 * Gets the id of the nearest osm object.
	 * 
	 * @return string
	 */
	public function getOsmId() : string
	{
		return $this->_osmId;
	}
	
	/**
	 * Sets the type of the nearest osm object.
	 * 
	 * @param string $osmType
	 * @return ApiOrgOpenstreetmapNominatimGeocodingResultInterface
	 */
	public function setOsmType(string $osmType) : ApiOrgOpenstreetmapNominatimGeocodingResultInterface
	{
		$this->_osmType = $osmType;
		
		return $this;
	}
	
	/**
	 * Gets the type of the nearest osm object.
	 * 
	 * @return string
	 */
	public function getOsmType() : string
	{
		return $this->_osmType;
	}
	
	/**
	 * Sets the id of the place this object is on.
	 * 
	 * @param string $placeId
	 * @return ApiOrgOpenstreetmapNominatimGeocodingResultInterface
	 */
	public function setPlaceId(string $placeId) : ApiOrgOpenstreetmapNominatimGeocodingResultInterface
	{
		$this->_placeId = $placeId;
		
		return $this;
	}
	
	/**
	 * Gets the id of the place this object is on.
	 * 
	 * @return string
	 */
	public function getPlaceId() : string
	{
		return $this->_placeId;
	}
	
	/**
	 * Sets the id of the place this object is ranked to.
	 * 
	 * @param string $placeRank
	 * @return ApiOrgOpenstreetmapNominatimGeocodingResultInterface
	 */
	public function setPlaceRank(string $placeRank) : ApiOrgOpenstreetmapNominatimGeocodingResultInterface
	{
		$this->_placeRank = $placeRank;
		
		return $this;
	}
	
	/**
	 * Gets the id of the place this object is ranked to.
	 * 
	 * @return string
	 */
	public function getPlaceRank() : string
	{
		return $this->_placeRank;
	}
	
	/**
	 * Sets the svg string that shapes this object.
	 * 
	 * @param string $svg
	 * @return ApiOrgOpenstreetmapNominatimGeocodingResultInterface
	 */
	public function setSvg(string $svg) : ApiOrgOpenstreetmapNominatimGeocodingResultInterface
	{
		$this->_svg = $svg;
		
		return $this;
	}
	
	/**
	 * Gets the svg string that shapes this object.
	 * 
	 * @return string
	 */
	public function getSvg() : string
	{
		return $this->_svg;
	}
	
	/**
	 * Sets the category of the object.
	 * 
	 * @param string $category
	 * @return ApiOrgOpenstreetmapNominatimGeocodingResultInterface
	 */
	public function setCategory(string $category) : ApiOrgOpenstreetmapNominatimGeocodingResultInterface
	{
		$this->_category = $category;
		
		return $this;
	}
	
	/**
	 * Gets the category of the object.
	 * 
	 * @return string
	 */
	public function getCategory() : string
	{
		return $this->_category;
	}
	
	/**
	 * Sets the type of object.
	 * 
	 * @param string $type
	 * @return ApiOrgOpenstreetmapNominatimGeocodingResultInterface
	 */
	public function setType(string $type) : ApiOrgOpenstreetmapNominatimGeocodingResultInterface
	{
		$this->_type = $type;
		
		return $this;
	}
	
	/**
	 * Gets the type of object.
	 * 
	 * @return string
	 */
	public function getType() : string
	{
		return $this->_type;
	}
	
	/**
	 * Sets the type of the address.
	 * 
	 * @param ?string $addresstype
	 * @return ApiOrgOpenstreetmapNominatimGeocodingResultInterface
	 */
	public function setAddresstype(?string $addresstype) : ApiOrgOpenstreetmapNominatimGeocodingResultInterface
	{
		$this->_addresstype = $addresstype;
		
		return $this;
	}
	
	/**
	 * Gets the type of the address.
	 * 
	 * @return ?string
	 */
	public function getAddresstype() : ?string
	{
		return $this->_addresstype;
	}
	
	/**
	 * Sets the icon for this place.
	 * 
	 * @param string $icon
	 * @return ApiOrgOpenstreetmapNominatimGeocodingResultInterface
	 */
	public function setIcon(string $icon) : ApiOrgOpenstreetmapNominatimGeocodingResultInterface
	{
		$this->_icon = $icon;
		
		return $this;
	}
	
	/**
	 * Gets the icon for this place.
	 * 
	 * @return string
	 */
	public function getIcon() : string
	{
		return $this->_icon;
	}
	
	/**
	 * Sets the geojson polygon, in case asked for.
	 * 
	 * @param ?GeoJsonGeometryInterface $geojson
	 * @return ApiOrgOpenstreetmapNominatimGeocodingResultInterface
	 */
	public function setGeojson(?GeoJsonGeometryInterface $geojson) : ApiOrgOpenstreetmapNominatimGeocodingResultInterface
	{
		$this->_geojson = $geojson;
		
		return $this;
	}
	
	/**
	 * Gets the geojson polygon, in case asked for.
	 * 
	 * @return ?GeoJsonGeometryInterface
	 */
	public function getGeojson() : ?GeoJsonGeometryInterface
	{
		return $this->_geojson;
	}
	
	/**
	 * Sets the geokml polygon, in case asked for.
	 * 
	 * @param ?string $geokml
	 * @return ApiOrgOpenstreetmapNominatimGeocodingResultInterface
	 */
	public function setGeokml(?string $geokml) : ApiOrgOpenstreetmapNominatimGeocodingResultInterface
	{
		$this->_geokml = $geokml;
		
		return $this;
	}
	
	/**
	 * Gets the geokml polygon, in case asked for.
	 * 
	 * @return ?string
	 */
	public function getGeokml() : ?string
	{
		return $this->_geokml;
	}
	
	/**
	 * Sets the geotext polygon, in case asked for.
	 * 
	 * @param ?string $geotext
	 * @return ApiOrgOpenstreetmapNominatimGeocodingResultInterface
	 */
	public function setGeotext(?string $geotext) : ApiOrgOpenstreetmapNominatimGeocodingResultInterface
	{
		$this->_geotext = $geotext;
		
		return $this;
	}
	
	/**
	 * Gets the geotext polygon, in case asked for.
	 * 
	 * @return ?string
	 */
	public function getGeotext() : ?string
	{
		return $this->_geotext;
	}
	
	/**
	 * Sets the name of the locality.
	 * 
	 * @param ?string $name
	 * @return ApiOrgOpenstreetmapNominatimGeocodingResultInterface
	 */
	public function setName(?string $name) : ApiOrgOpenstreetmapNominatimGeocodingResultInterface
	{
		$this->_name = $name;
		
		return $this;
	}
	
	/**
	 * Gets the name of the locality.
	 * 
	 * @return ?string
	 */
	public function getName() : ?string
	{
		return $this->_name;
	}
	
	/**
	 * Sets the name details, if requested.
	 * 
	 * @param ?ApiOrgOpenstreetmapNominatimNameDetailsInterface $nameDetails
	 * @return ApiOrgOpenstreetmapNominatimGeocodingResultInterface
	 */
	public function setNameDetails(?ApiOrgOpenstreetmapNominatimNameDetailsInterface $nameDetails) : ApiOrgOpenstreetmapNominatimGeocodingResultInterface
	{
		$this->_nameDetails = $nameDetails;
		
		return $this;
	}
	
	/**
	 * Gets the name details, if requested.
	 * 
	 * @return ?ApiOrgOpenstreetmapNominatimNameDetailsInterface
	 */
	public function getNameDetails() : ?ApiOrgOpenstreetmapNominatimNameDetailsInterface
	{
		return $this->_nameDetails;
	}
	
}
