<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-org-openstreetmap-nominatim-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Osm;

use InvalidArgumentException;
use Iterator;
use PhpExtended\DataProvider\JsonStringDataProvider;
use PhpExtended\DataProvider\UnprovidableThrowable;
use PhpExtended\GeoJson\GeoJsonReifierConfiguration;
use PhpExtended\HttpMessage\RequestFactory;
use PhpExtended\HttpMessage\StreamFactory;
use PhpExtended\HttpMessage\UriFactory;
use PhpExtended\Reifier\ReificationThrowable;
use PhpExtended\Reifier\Reifier;
use PhpExtended\Reifier\ReifierInterface;
use Psr\Http\Client\ClientExceptionInterface;
use Psr\Http\Client\ClientInterface;
use Psr\Http\Message\RequestFactoryInterface;
use Psr\Http\Message\StreamFactoryInterface;
use Psr\Http\Message\UriFactoryInterface;

/**
 * OpenStreetMapNominatimApiEndpoint class file.
 * 
 * This class is a simple implementation of the ApiOrgOpenstreetmapNominatimEndpointInterface.
 * 
 * @author Anastaszor
 * @SuppressWarnings("PHPMD.CouplingBetweenObjects")
 */
class ApiOrgOpenstreetmapNominatimEndpoint implements ApiOrgOpenstreetmapNominatimEndpointInterface
{
	
	public const HOST = 'https://nominatim.openstreetmap.org/';
	
	/**
	 * The http client.
	 *
	 * @var ClientInterface
	 */
	protected ClientInterface $_httpClient;
	
	/**
	 * The uri factory.
	 *
	 * @var UriFactoryInterface
	 */
	protected UriFactoryInterface $_uriFactory;
	
	/**
	 * The request factory.
	 *
	 * @var RequestFactoryInterface
	 */
	protected RequestFactoryInterface $_requestFactory;
	
	/**
	 * The stream factory.
	 *
	 * @var StreamFactoryInterface
	 */
	protected StreamFactoryInterface $_streamFactory;
	
	/**
	 * The reifier.
	 *
	 * @var ReifierInterface
	 */
	protected ReifierInterface $_reifier;
	
	/**
	 * Builds a new endpoint with its dependancies.
	 *
	 * @param ClientInterface $client
	 * @param ?UriFactoryInterface $uriFactory
	 * @param ?RequestFactoryInterface $requestFactory
	 * @param ?StreamFactoryInterface $streamFactory
	 * @param ?ReifierInterface $reifier
	 * @throws InvalidArgumentException
	 */
	public function __construct(
		ClientInterface $client,
		?UriFactoryInterface $uriFactory = null,
		?RequestFactoryInterface $requestFactory = null,
		?StreamFactoryInterface $streamFactory = null,
		?ReifierInterface $reifier = null
	) {
		$this->_httpClient = $client;
		$this->_uriFactory = $uriFactory ?? new UriFactory();
		$this->_requestFactory = $requestFactory ?? new RequestFactory();
		$this->_streamFactory = $streamFactory ?? new StreamFactory();
		$this->_reifier = $reifier ?? new Reifier();
		$this->_reifier->setConfiguration(new GeoJsonReifierConfiguration());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Osm\ApiOrgOpenstreetmapNominatimEndpointInterface::searchGeocode()
	 * @throws ClientExceptionInterface
	 * @throws InvalidArgumentException should not happen
	 * @throws ReificationThrowable
	 * @throws UnprovidableThrowable
	 * @SuppressWarnings("PHPMD.CyclomaticComplexity")
	 * @SuppressWarnings("PHPMD.NPathComplexity")
	 * @SuppressWarnings("PHPMD.ExcessiveMethodLength")
	 */
	public function searchGeocode(ApiOrgOpenstreetmapNominatimRequestInterface $request) : Iterator
	{
		$requestArr = [
			'format' => 'json',
		];
		
		$acceptLanguage = $request->getAcceptLanguage();
		if(null !== $acceptLanguage)
		{
			$requestArr['accept_language'] = $acceptLanguage->getHeaderValue();
		}
		if(null !== $request->getQuery())
		{
			$requestArr['q'] = $request->getQuery();
		}
		if(null !== $request->getStreet())
		{
			$requestArr['street'] = $request->getStreet();
		}
		if(null !== $request->getPostalCode())
		{
			$requestArr['postal_code'] = $request->getPostalCode();
		}
		if(null !== $request->getCounty())
		{
			$requestArr['county'] = $request->getCounty();
		}
		if(null !== $request->getState())
		{
			$requestArr['state'] = $request->getState();
		}
		if(null !== $request->getCountry())
		{
			$requestArr['country'] = $request->getCountry();
		}
		if(!empty($request->getCountryCodes()))
		{
			$requestArr['country_codes'] = \implode(',', $request->getCountryCodes());
		}
		if(!empty($request->getViewbox()))
		{
			$requestArr['viewbox'] = \implode(',', $request->getViewbox());
		}
		if(null !== $request->hasBounded())
		{
			$requestArr['bounded'] = $request->hasBounded();
		}
		if(null !== $request->hasAddressDetails())
		{
			$requestArr['address_details'] = $request->hasAddressDetails();
		}
		$email = $request->getEmail();
		if(null !== $email)
		{
			$requestArr['email'] = $email->getCanonicalRepresentation();
		}
		if(!empty($request->getExcludePlaceIds()))
		{
			$requestArr['exclude_place_ids'] = \implode(',', $request->getExcludePlaceIds());
		}
		$requestArr['limit'] = $request->getLimit() ?? 10;
		if(null !== $request->hasDedupe())
		{
			$requestArr['dedupe'] = $request->hasDedupe();
		}
		if(null !== $request->hasDebug())
		{
			$requestArr['debug'] = $request->hasDebug();
		}
		if(null !== $request->hasPolygonGeojson())
		{
			$requestArr['polygon_geojson'] = $request->hasPolygonGeojson();
		}
		if(null !== $request->hasPolygonKml())
		{
			$requestArr['polygon_kml'] = $request->hasPolygonKml();
		}
		if(null !== $request->hasPolygonSvg())
		{
			$requestArr['polygon_svg'] = $request->hasPolygonSvg();
		}
		if(null !== $request->hasPolygonText())
		{
			$requestArr['polygon_text'] = $request->hasPolygonText();
		}
		if(null !== $request->hasExtraTags())
		{
			$requestArr['extra_tags'] = $request->hasExtraTags();
		}
		if(null !== $request->hasNameDetails())
		{
			$requestArr['name_details'] = $request->hasNameDetails();
		}
		
		$uri = $this->_uriFactory->createUri(self::HOST.'search?'.\http_build_query($requestArr));
		$request = $this->_requestFactory->createRequest('GET', $uri);
		$response = $this->_httpClient->sendRequest($request);
		$json = new JsonStringDataProvider($response->getBody()->__toString());
		
		return $this->_reifier->reifyIterator(ApiOrgOpenstreetmapNominatimGeocodingResult::class, $json->provideIterator());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Osm\ApiOrgOpenstreetmapNominatimEndpointInterface::searchReverseGeocode()
	 * @throws ClientExceptionInterface
	 * @throws InvalidArgumentException should not happen
	 * @throws ReificationThrowable
	 * @throws UnprovidableThrowable
	 * @SuppressWarnings("PHPMD.CyclomaticComplexity")
	 * @SuppressWarnings("PHPMD.NPathComplexity")
	 */
	public function searchReverseGeocode(ApiOrgOpenstreetmapNominatimReverseRequestInterface $request) : ApiOrgOpenstreetmapNominatimGeocodingResultInterface
	{
		$requestArr = [
			'format' => 'json',
		];
		
		$acceptLanguage = $request->getAcceptLanguage();
		if(null !== $acceptLanguage)
		{
			$requestArr['accept_language'] = $acceptLanguage->getHeaderValue();
		}
		if(null !== $request->getOsmType())
		{
			$requestArr['osm_type'] = $request->getOsmType();
		}
		if(null !== $request->getOsmId())
		{
			$requestArr['osm_id'] = $request->getOsmId();
		}
		if(null !== $request->getLatitude())
		{
			$requestArr['lat'] = $request->getLatitude();
		}
		if(null !== $request->getLongitude())
		{
			$requestArr['lon'] = $request->getLongitude();
		}
		if(null !== $request->getZoom())
		{
			$requestArr['zoom'] = $request->getZoom();
		}
		if(null !== $request->hasAddressDetails())
		{
			$requestArr['address_details'] = $request->hasAddressDetails();
		}
		$email = $request->getEmail();
		if(null !== $email)
		{
			$requestArr['email'] = $email->__toString();
		}
		if(null !== $request->hasPolygonGeojson())
		{
			$requestArr['polygon_geojson'] = $request->hasPolygonGeojson();
		}
		if(null !== $request->hasPolygonKml())
		{
			$requestArr['polygon_kml'] = $request->hasPolygonKml();
		}
		if(null !== $request->hasPolygonSvg())
		{
			$requestArr['polygon_svg'] = $request->hasPolygonSvg();
		}
		if(null !== $request->hasPolygonText())
		{
			$requestArr['polygon_text'] = $request->hasPolygonText();
		}
		if(null !== $request->hasExtraTags())
		{
			$requestArr['extra_tags'] = $request->hasExtraTags();
		}
		if(null !== $request->hasNameDetails())
		{
			$requestArr['name_details'] = $request->hasNameDetails();
		}
		
		$uri = $this->_uriFactory->createUri(self::HOST.'reverse?'.\http_build_query($requestArr));
		$request = $this->_requestFactory->createRequest('GET', $uri);
		$response = $this->_httpClient->sendRequest($request);
		$json = new JsonStringDataProvider($response->getBody()->__toString());
		
		return $this->_reifier->reify(ApiOrgOpenstreetmapNominatimGeocodingResult::class, $json->provideOne());
	}
	
}
