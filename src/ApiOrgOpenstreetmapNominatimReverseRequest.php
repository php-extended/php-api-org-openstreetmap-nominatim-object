<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-org-openstreetmap-nominatim-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Osm;

use PhpExtended\Email\EmailAddressInterface;
use PhpExtended\HttpClient\AcceptLanguageChainInterface;

/**
 * ApiOrgOpenstreetmapNominatimReverseRequest class file.
 * 
 * This is a simple implementation of the
 * ApiOrgOpenstreetmapNominatimReverseRequestInterface.
 * 
 * /!\ This file was generated automatically from the json-schema.json file.
 * /!\ Do not edit by hand or the modifications will be erased.
 * @generator PhpExtended\JsonSchema\Php74ClassMetadata
 * 
 * @author Anastaszor
 * @SuppressWarnings("PHPMD.LongClassName")
 */
class ApiOrgOpenstreetmapNominatimReverseRequest implements ApiOrgOpenstreetmapNominatimReverseRequestInterface
{
	
	/**
	 * Preferred language order for showing search results, overrides the value
	 * specified in the "Accept-Language" HTTP header. Either uses standard
	 * rfc2616 accept-language string or a simple comma separated list of
	 * language codes.
	 * 
	 * @var ?AcceptLanguageChainInterface
	 */
	protected ?AcceptLanguageChainInterface $_acceptLanguage = null;
	
	/**
	 * A specific osm node / way / relation to return an address for. Please
	 * use this in preference to lat/lon where possible.
	 * 
	 * @var ?string
	 */
	protected ?string $_osmType = null;
	
	/**
	 * The id of the osm object.
	 * 
	 * @var ?string
	 */
	protected ?string $_osmId = null;
	
	/**
	 * The location to generate an address for.
	 * 
	 * @var ?float
	 */
	protected ?float $_latitude = null;
	
	/**
	 * The location to generate an address for.
	 * 
	 * @var ?float
	 */
	protected ?float $_longitude = null;
	
	/**
	 * Level of detail required where 0 is country and 18 is house/building.
	 * 
	 * @var ?int
	 */
	protected ?int $_zoom = null;
	
	/**
	 * Whether to include a breakdown of the address into elements.
	 * 
	 * @var ?bool
	 */
	protected ?bool $_addressDetails = null;
	
	/**
	 * If you are making large numbers of request please include a valid email
	 * address or alternatively include your email address as part of the
	 * User-Agent string. This information will be kept confidential and only
	 * used to contact you in the event of a problem, see Usage Policy for more
	 * details.
	 * 
	 * @var ?EmailAddressInterface
	 */
	protected ?EmailAddressInterface $_email = null;
	
	/**
	 * Whether to output geometry of results in geojson format.
	 * 
	 * @var ?bool
	 */
	protected ?bool $_polygonGeojson = null;
	
	/**
	 * Whether to output geometry of results in kml format.
	 * 
	 * @var ?bool
	 */
	protected ?bool $_polygonKml = null;
	
	/**
	 * Whether to output geometry of results in svg format.
	 * 
	 * @var ?bool
	 */
	protected ?bool $_polygonSvg = null;
	
	/**
	 * Whether to output geometry of results as a WKT.
	 * 
	 * @var ?bool
	 */
	protected ?bool $_polygonText = null;
	
	/**
	 * Whether to include additional information in the result if available,
	 * e.g. wikipedia link, opening hours.
	 * 
	 * @var ?bool
	 */
	protected ?bool $_extraTags = null;
	
	/**
	 * Include a list of alternative names in the results. These may include
	 * language variants, references, operator and brand.
	 * 
	 * @var ?bool
	 */
	protected ?bool $_nameDetails = null;
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * Sets preferred language order for showing search results, overrides the
	 * value specified in the "Accept-Language" HTTP header. Either uses
	 * standard rfc2616 accept-language string or a simple comma separated list
	 * of language codes.
	 * 
	 * @param ?AcceptLanguageChainInterface $acceptLanguage
	 * @return ApiOrgOpenstreetmapNominatimReverseRequestInterface
	 */
	public function setAcceptLanguage(?AcceptLanguageChainInterface $acceptLanguage) : ApiOrgOpenstreetmapNominatimReverseRequestInterface
	{
		$this->_acceptLanguage = $acceptLanguage;
		
		return $this;
	}
	
	/**
	 * Gets preferred language order for showing search results, overrides the
	 * value specified in the "Accept-Language" HTTP header. Either uses
	 * standard rfc2616 accept-language string or a simple comma separated list
	 * of language codes.
	 * 
	 * @return ?AcceptLanguageChainInterface
	 */
	public function getAcceptLanguage() : ?AcceptLanguageChainInterface
	{
		return $this->_acceptLanguage;
	}
	
	/**
	 * Sets a specific osm node / way / relation to return an address for.
	 * Please use this in preference to lat/lon where possible.
	 * 
	 * @param ?string $osmType
	 * @return ApiOrgOpenstreetmapNominatimReverseRequestInterface
	 */
	public function setOsmType(?string $osmType) : ApiOrgOpenstreetmapNominatimReverseRequestInterface
	{
		$this->_osmType = $osmType;
		
		return $this;
	}
	
	/**
	 * Gets a specific osm node / way / relation to return an address for.
	 * Please use this in preference to lat/lon where possible.
	 * 
	 * @return ?string
	 */
	public function getOsmType() : ?string
	{
		return $this->_osmType;
	}
	
	/**
	 * Sets the id of the osm object.
	 * 
	 * @param ?string $osmId
	 * @return ApiOrgOpenstreetmapNominatimReverseRequestInterface
	 */
	public function setOsmId(?string $osmId) : ApiOrgOpenstreetmapNominatimReverseRequestInterface
	{
		$this->_osmId = $osmId;
		
		return $this;
	}
	
	/**
	 * Gets the id of the osm object.
	 * 
	 * @return ?string
	 */
	public function getOsmId() : ?string
	{
		return $this->_osmId;
	}
	
	/**
	 * Sets the location to generate an address for.
	 * 
	 * @param ?float $latitude
	 * @return ApiOrgOpenstreetmapNominatimReverseRequestInterface
	 */
	public function setLatitude(?float $latitude) : ApiOrgOpenstreetmapNominatimReverseRequestInterface
	{
		$this->_latitude = $latitude;
		
		return $this;
	}
	
	/**
	 * Gets the location to generate an address for.
	 * 
	 * @return ?float
	 */
	public function getLatitude() : ?float
	{
		return $this->_latitude;
	}
	
	/**
	 * Sets the location to generate an address for.
	 * 
	 * @param ?float $longitude
	 * @return ApiOrgOpenstreetmapNominatimReverseRequestInterface
	 */
	public function setLongitude(?float $longitude) : ApiOrgOpenstreetmapNominatimReverseRequestInterface
	{
		$this->_longitude = $longitude;
		
		return $this;
	}
	
	/**
	 * Gets the location to generate an address for.
	 * 
	 * @return ?float
	 */
	public function getLongitude() : ?float
	{
		return $this->_longitude;
	}
	
	/**
	 * Sets level of detail required where 0 is country and 18 is
	 * house/building.
	 * 
	 * @param ?int $zoom
	 * @return ApiOrgOpenstreetmapNominatimReverseRequestInterface
	 */
	public function setZoom(?int $zoom) : ApiOrgOpenstreetmapNominatimReverseRequestInterface
	{
		$this->_zoom = $zoom;
		
		return $this;
	}
	
	/**
	 * Gets level of detail required where 0 is country and 18 is
	 * house/building.
	 * 
	 * @return ?int
	 */
	public function getZoom() : ?int
	{
		return $this->_zoom;
	}
	
	/**
	 * Sets whether to include a breakdown of the address into elements.
	 * 
	 * @param ?bool $addressDetails
	 * @return ApiOrgOpenstreetmapNominatimReverseRequestInterface
	 */
	public function setAddressDetails(?bool $addressDetails) : ApiOrgOpenstreetmapNominatimReverseRequestInterface
	{
		$this->_addressDetails = $addressDetails;
		
		return $this;
	}
	
	/**
	 * Gets whether to include a breakdown of the address into elements.
	 * 
	 * @return ?bool
	 */
	public function hasAddressDetails() : ?bool
	{
		return $this->_addressDetails;
	}
	
	/**
	 * Sets if you are making large numbers of request please include a valid
	 * email address or alternatively include your email address as part of the
	 * User-Agent string. This information will be kept confidential and only
	 * used to contact you in the event of a problem, see Usage Policy for more
	 * details.
	 * 
	 * @param ?EmailAddressInterface $email
	 * @return ApiOrgOpenstreetmapNominatimReverseRequestInterface
	 */
	public function setEmail(?EmailAddressInterface $email) : ApiOrgOpenstreetmapNominatimReverseRequestInterface
	{
		$this->_email = $email;
		
		return $this;
	}
	
	/**
	 * Gets if you are making large numbers of request please include a valid
	 * email address or alternatively include your email address as part of the
	 * User-Agent string. This information will be kept confidential and only
	 * used to contact you in the event of a problem, see Usage Policy for more
	 * details.
	 * 
	 * @return ?EmailAddressInterface
	 */
	public function getEmail() : ?EmailAddressInterface
	{
		return $this->_email;
	}
	
	/**
	 * Sets whether to output geometry of results in geojson format.
	 * 
	 * @param ?bool $polygonGeojson
	 * @return ApiOrgOpenstreetmapNominatimReverseRequestInterface
	 */
	public function setPolygonGeojson(?bool $polygonGeojson) : ApiOrgOpenstreetmapNominatimReverseRequestInterface
	{
		$this->_polygonGeojson = $polygonGeojson;
		
		return $this;
	}
	
	/**
	 * Gets whether to output geometry of results in geojson format.
	 * 
	 * @return ?bool
	 */
	public function hasPolygonGeojson() : ?bool
	{
		return $this->_polygonGeojson;
	}
	
	/**
	 * Sets whether to output geometry of results in kml format.
	 * 
	 * @param ?bool $polygonKml
	 * @return ApiOrgOpenstreetmapNominatimReverseRequestInterface
	 */
	public function setPolygonKml(?bool $polygonKml) : ApiOrgOpenstreetmapNominatimReverseRequestInterface
	{
		$this->_polygonKml = $polygonKml;
		
		return $this;
	}
	
	/**
	 * Gets whether to output geometry of results in kml format.
	 * 
	 * @return ?bool
	 */
	public function hasPolygonKml() : ?bool
	{
		return $this->_polygonKml;
	}
	
	/**
	 * Sets whether to output geometry of results in svg format.
	 * 
	 * @param ?bool $polygonSvg
	 * @return ApiOrgOpenstreetmapNominatimReverseRequestInterface
	 */
	public function setPolygonSvg(?bool $polygonSvg) : ApiOrgOpenstreetmapNominatimReverseRequestInterface
	{
		$this->_polygonSvg = $polygonSvg;
		
		return $this;
	}
	
	/**
	 * Gets whether to output geometry of results in svg format.
	 * 
	 * @return ?bool
	 */
	public function hasPolygonSvg() : ?bool
	{
		return $this->_polygonSvg;
	}
	
	/**
	 * Sets whether to output geometry of results as a WKT.
	 * 
	 * @param ?bool $polygonText
	 * @return ApiOrgOpenstreetmapNominatimReverseRequestInterface
	 */
	public function setPolygonText(?bool $polygonText) : ApiOrgOpenstreetmapNominatimReverseRequestInterface
	{
		$this->_polygonText = $polygonText;
		
		return $this;
	}
	
	/**
	 * Gets whether to output geometry of results as a WKT.
	 * 
	 * @return ?bool
	 */
	public function hasPolygonText() : ?bool
	{
		return $this->_polygonText;
	}
	
	/**
	 * Sets whether to include additional information in the result if
	 * available, e.g. wikipedia link, opening hours.
	 * 
	 * @param ?bool $extraTags
	 * @return ApiOrgOpenstreetmapNominatimReverseRequestInterface
	 */
	public function setExtraTags(?bool $extraTags) : ApiOrgOpenstreetmapNominatimReverseRequestInterface
	{
		$this->_extraTags = $extraTags;
		
		return $this;
	}
	
	/**
	 * Gets whether to include additional information in the result if
	 * available, e.g. wikipedia link, opening hours.
	 * 
	 * @return ?bool
	 */
	public function hasExtraTags() : ?bool
	{
		return $this->_extraTags;
	}
	
	/**
	 * Sets include a list of alternative names in the results. These may
	 * include language variants, references, operator and brand.
	 * 
	 * @param ?bool $nameDetails
	 * @return ApiOrgOpenstreetmapNominatimReverseRequestInterface
	 */
	public function setNameDetails(?bool $nameDetails) : ApiOrgOpenstreetmapNominatimReverseRequestInterface
	{
		$this->_nameDetails = $nameDetails;
		
		return $this;
	}
	
	/**
	 * Gets include a list of alternative names in the results. These may
	 * include language variants, references, operator and brand.
	 * 
	 * @return ?bool
	 */
	public function hasNameDetails() : ?bool
	{
		return $this->_nameDetails;
	}
	
}
