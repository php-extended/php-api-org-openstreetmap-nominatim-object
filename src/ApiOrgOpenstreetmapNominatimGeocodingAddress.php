<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-org-openstreetmap-nominatim-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Osm;

/**
 * ApiOrgOpenstreetmapNominatimGeocodingAddress class file.
 * 
 * This is a simple implementation of the
 * ApiOrgOpenstreetmapNominatimGeocodingAddressInterface.
 * 
 * /!\ This file was generated automatically from the json-schema.json file.
 * /!\ Do not edit by hand or the modifications will be erased.
 * @generator PhpExtended\JsonSchema\Php74ClassMetadata
 * 
 * @author Anastaszor
 * @SuppressWarnings("PHPMD.LongClassName")
 * @SuppressWarnings("PHPMD.TooManyFields")
 */
class ApiOrgOpenstreetmapNominatimGeocodingAddress implements ApiOrgOpenstreetmapNominatimGeocodingAddressInterface
{
	
	/**
	 * The amenity.
	 * 
	 * @var ?string
	 */
	protected ?string $_amenity = null;
	
	/**
	 * The borough (arrondissement).
	 * 
	 * @var ?string
	 */
	protected ?string $_borough = null;
	
	/**
	 * The number of the area in the street.
	 * 
	 * @var ?string
	 */
	protected ?string $_houseNumber = null;
	
	/**
	 * The road.
	 * 
	 * @var ?string
	 */
	protected ?string $_road = null;
	
	/**
	 * The quarter.
	 * 
	 * @var ?string
	 */
	protected ?string $_quarter = null;
	
	/**
	 * The neighbourhood name.
	 * 
	 * @var ?string
	 */
	protected ?string $_neighbourhood = null;
	
	/**
	 * The district.
	 * 
	 * @var ?string
	 */
	protected ?string $_district = null;
	
	/**
	 * The suburban area name.
	 * 
	 * @var ?string
	 */
	protected ?string $_suburb = null;
	
	/**
	 * The county.
	 * 
	 * @var ?string
	 */
	protected ?string $_county = null;
	
	/**
	 * The city name.
	 * 
	 * @var ?string
	 */
	protected ?string $_city = null;
	
	/**
	 * The city block name.
	 * 
	 * @var ?string
	 */
	protected ?string $_cityBlock = null;
	
	/**
	 * The state name.
	 * 
	 * @var ?string
	 */
	protected ?string $_state = null;
	
	/**
	 * The postal code.
	 * 
	 * @var ?string
	 */
	protected ?string $_postCode = null;
	
	/**
	 * The country name.
	 * 
	 * @var ?string
	 */
	protected ?string $_country = null;
	
	/**
	 * The country code.
	 * 
	 * @var ?string
	 */
	protected ?string $_countryCode = null;
	
	/**
	 * The ISO 3166-2 lvl4 value.
	 * 
	 * @var ?string
	 */
	protected ?string $_iso31662lvl4 = null;
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * Sets the amenity.
	 * 
	 * @param ?string $amenity
	 * @return ApiOrgOpenstreetmapNominatimGeocodingAddressInterface
	 */
	public function setAmenity(?string $amenity) : ApiOrgOpenstreetmapNominatimGeocodingAddressInterface
	{
		$this->_amenity = $amenity;
		
		return $this;
	}
	
	/**
	 * Gets the amenity.
	 * 
	 * @return ?string
	 */
	public function getAmenity() : ?string
	{
		return $this->_amenity;
	}
	
	/**
	 * Sets the borough (arrondissement).
	 * 
	 * @param ?string $borough
	 * @return ApiOrgOpenstreetmapNominatimGeocodingAddressInterface
	 */
	public function setBorough(?string $borough) : ApiOrgOpenstreetmapNominatimGeocodingAddressInterface
	{
		$this->_borough = $borough;
		
		return $this;
	}
	
	/**
	 * Gets the borough (arrondissement).
	 * 
	 * @return ?string
	 */
	public function getBorough() : ?string
	{
		return $this->_borough;
	}
	
	/**
	 * Sets the number of the area in the street.
	 * 
	 * @param ?string $houseNumber
	 * @return ApiOrgOpenstreetmapNominatimGeocodingAddressInterface
	 */
	public function setHouseNumber(?string $houseNumber) : ApiOrgOpenstreetmapNominatimGeocodingAddressInterface
	{
		$this->_houseNumber = $houseNumber;
		
		return $this;
	}
	
	/**
	 * Gets the number of the area in the street.
	 * 
	 * @return ?string
	 */
	public function getHouseNumber() : ?string
	{
		return $this->_houseNumber;
	}
	
	/**
	 * Sets the road.
	 * 
	 * @param ?string $road
	 * @return ApiOrgOpenstreetmapNominatimGeocodingAddressInterface
	 */
	public function setRoad(?string $road) : ApiOrgOpenstreetmapNominatimGeocodingAddressInterface
	{
		$this->_road = $road;
		
		return $this;
	}
	
	/**
	 * Gets the road.
	 * 
	 * @return ?string
	 */
	public function getRoad() : ?string
	{
		return $this->_road;
	}
	
	/**
	 * Sets the quarter.
	 * 
	 * @param ?string $quarter
	 * @return ApiOrgOpenstreetmapNominatimGeocodingAddressInterface
	 */
	public function setQuarter(?string $quarter) : ApiOrgOpenstreetmapNominatimGeocodingAddressInterface
	{
		$this->_quarter = $quarter;
		
		return $this;
	}
	
	/**
	 * Gets the quarter.
	 * 
	 * @return ?string
	 */
	public function getQuarter() : ?string
	{
		return $this->_quarter;
	}
	
	/**
	 * Sets the neighbourhood name.
	 * 
	 * @param ?string $neighbourhood
	 * @return ApiOrgOpenstreetmapNominatimGeocodingAddressInterface
	 */
	public function setNeighbourhood(?string $neighbourhood) : ApiOrgOpenstreetmapNominatimGeocodingAddressInterface
	{
		$this->_neighbourhood = $neighbourhood;
		
		return $this;
	}
	
	/**
	 * Gets the neighbourhood name.
	 * 
	 * @return ?string
	 */
	public function getNeighbourhood() : ?string
	{
		return $this->_neighbourhood;
	}
	
	/**
	 * Sets the district.
	 * 
	 * @param ?string $district
	 * @return ApiOrgOpenstreetmapNominatimGeocodingAddressInterface
	 */
	public function setDistrict(?string $district) : ApiOrgOpenstreetmapNominatimGeocodingAddressInterface
	{
		$this->_district = $district;
		
		return $this;
	}
	
	/**
	 * Gets the district.
	 * 
	 * @return ?string
	 */
	public function getDistrict() : ?string
	{
		return $this->_district;
	}
	
	/**
	 * Sets the suburban area name.
	 * 
	 * @param ?string $suburb
	 * @return ApiOrgOpenstreetmapNominatimGeocodingAddressInterface
	 */
	public function setSuburb(?string $suburb) : ApiOrgOpenstreetmapNominatimGeocodingAddressInterface
	{
		$this->_suburb = $suburb;
		
		return $this;
	}
	
	/**
	 * Gets the suburban area name.
	 * 
	 * @return ?string
	 */
	public function getSuburb() : ?string
	{
		return $this->_suburb;
	}
	
	/**
	 * Sets the county.
	 * 
	 * @param ?string $county
	 * @return ApiOrgOpenstreetmapNominatimGeocodingAddressInterface
	 */
	public function setCounty(?string $county) : ApiOrgOpenstreetmapNominatimGeocodingAddressInterface
	{
		$this->_county = $county;
		
		return $this;
	}
	
	/**
	 * Gets the county.
	 * 
	 * @return ?string
	 */
	public function getCounty() : ?string
	{
		return $this->_county;
	}
	
	/**
	 * Sets the city name.
	 * 
	 * @param ?string $city
	 * @return ApiOrgOpenstreetmapNominatimGeocodingAddressInterface
	 */
	public function setCity(?string $city) : ApiOrgOpenstreetmapNominatimGeocodingAddressInterface
	{
		$this->_city = $city;
		
		return $this;
	}
	
	/**
	 * Gets the city name.
	 * 
	 * @return ?string
	 */
	public function getCity() : ?string
	{
		return $this->_city;
	}
	
	/**
	 * Sets the city block name.
	 * 
	 * @param ?string $cityBlock
	 * @return ApiOrgOpenstreetmapNominatimGeocodingAddressInterface
	 */
	public function setCityBlock(?string $cityBlock) : ApiOrgOpenstreetmapNominatimGeocodingAddressInterface
	{
		$this->_cityBlock = $cityBlock;
		
		return $this;
	}
	
	/**
	 * Gets the city block name.
	 * 
	 * @return ?string
	 */
	public function getCityBlock() : ?string
	{
		return $this->_cityBlock;
	}
	
	/**
	 * Sets the state name.
	 * 
	 * @param ?string $state
	 * @return ApiOrgOpenstreetmapNominatimGeocodingAddressInterface
	 */
	public function setState(?string $state) : ApiOrgOpenstreetmapNominatimGeocodingAddressInterface
	{
		$this->_state = $state;
		
		return $this;
	}
	
	/**
	 * Gets the state name.
	 * 
	 * @return ?string
	 */
	public function getState() : ?string
	{
		return $this->_state;
	}
	
	/**
	 * Sets the postal code.
	 * 
	 * @param ?string $postCode
	 * @return ApiOrgOpenstreetmapNominatimGeocodingAddressInterface
	 */
	public function setPostCode(?string $postCode) : ApiOrgOpenstreetmapNominatimGeocodingAddressInterface
	{
		$this->_postCode = $postCode;
		
		return $this;
	}
	
	/**
	 * Gets the postal code.
	 * 
	 * @return ?string
	 */
	public function getPostCode() : ?string
	{
		return $this->_postCode;
	}
	
	/**
	 * Sets the country name.
	 * 
	 * @param ?string $country
	 * @return ApiOrgOpenstreetmapNominatimGeocodingAddressInterface
	 */
	public function setCountry(?string $country) : ApiOrgOpenstreetmapNominatimGeocodingAddressInterface
	{
		$this->_country = $country;
		
		return $this;
	}
	
	/**
	 * Gets the country name.
	 * 
	 * @return ?string
	 */
	public function getCountry() : ?string
	{
		return $this->_country;
	}
	
	/**
	 * Sets the country code.
	 * 
	 * @param ?string $countryCode
	 * @return ApiOrgOpenstreetmapNominatimGeocodingAddressInterface
	 */
	public function setCountryCode(?string $countryCode) : ApiOrgOpenstreetmapNominatimGeocodingAddressInterface
	{
		$this->_countryCode = $countryCode;
		
		return $this;
	}
	
	/**
	 * Gets the country code.
	 * 
	 * @return ?string
	 */
	public function getCountryCode() : ?string
	{
		return $this->_countryCode;
	}
	
	/**
	 * Sets the ISO 3166-2 lvl4 value.
	 * 
	 * @param ?string $iso31662lvl4
	 * @return ApiOrgOpenstreetmapNominatimGeocodingAddressInterface
	 */
	public function setIso31662lvl4(?string $iso31662lvl4) : ApiOrgOpenstreetmapNominatimGeocodingAddressInterface
	{
		$this->_iso31662lvl4 = $iso31662lvl4;
		
		return $this;
	}
	
	/**
	 * Gets the ISO 3166-2 lvl4 value.
	 * 
	 * @return ?string
	 */
	public function getIso31662lvl4() : ?string
	{
		return $this->_iso31662lvl4;
	}
	
}
