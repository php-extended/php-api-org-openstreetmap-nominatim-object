<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-org-openstreetmap-nominatim-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Osm;

use PhpExtended\Email\EmailAddressInterface;
use PhpExtended\HttpClient\AcceptLanguageChainInterface;

/**
 * ApiOrgOpenstreetmapNominatimRequest class file.
 * 
 * This is a simple implementation of the
 * ApiOrgOpenstreetmapNominatimRequestInterface.
 * 
 * /!\ This file was generated automatically from the json-schema.json file.
 * /!\ Do not edit by hand or the modifications will be erased.
 * @generator PhpExtended\JsonSchema\Php74ClassMetadata
 * 
 * @author Anastaszor
 * @SuppressWarnings("PHPMD.ExcessivePublicCount")
 * @SuppressWarnings("PHPMD.TooManyFields")
 * @SuppressWarnings("PHPMD.TooManyPublicMethods")
 */
class ApiOrgOpenstreetmapNominatimRequest implements ApiOrgOpenstreetmapNominatimRequestInterface
{
	
	/**
	 * Preferred language order for showing search results, overrides the value
	 * specified in the "Accept-Language" HTTP header. Either uses standard
	 * rfc2616 accept-language string or a simple comma separated list of
	 * language codes.
	 * 
	 * @var ?AcceptLanguageChainInterface
	 */
	protected ?AcceptLanguageChainInterface $_acceptLanguage = null;
	
	/**
	 * Query string to search for. If this is set, the values for all the
	 * attributes {street, postal_code, city, county, state, country} are
	 * erased.
	 * 
	 * @var ?string
	 */
	protected ?string $_query = null;
	
	/**
	 * Street number and street name.
	 * 
	 * @var ?string
	 */
	protected ?string $_street = null;
	
	/**
	 * The postal code of the city.
	 * 
	 * @var ?string
	 */
	protected ?string $_postalCode = null;
	
	/**
	 * The name of the city.
	 * 
	 * @var ?string
	 */
	protected ?string $_city = null;
	
	/**
	 * The county of the query.
	 * 
	 * @var ?string
	 */
	protected ?string $_county = null;
	
	/**
	 * The state of the query.
	 * 
	 * @var ?string
	 */
	protected ?string $_state = null;
	
	/**
	 * The country of the query.
	 * 
	 * @var ?string
	 */
	protected ?string $_country = null;
	
	/**
	 * Limit search results to a specific country (or a list of countries).
	 * <countrycode> should be the ISO 3166-1alpha2 code, e.g. gb for the
	 * United Kingdom, de for Germany, etc.
	 * 
	 * @var array<int, string>
	 */
	protected array $_countryCodes = [];
	
	/**
	 * The preferred area to find search results. Any two corner points of the
	 * box are accepted in any order as long as they span a real box.
	 * 
	 * @var array<int, int>
	 */
	protected array $_viewbox = [];
	
	/**
	 * Restrict the results to only items contained with the viewbox (see
	 * above).
	 * Restricting the results to the bounding box also enables searching by
	 * amenity only. For example a search query of just "[pub]" would normally
	 * be rejected but with bounded=1 will result in a list of items matching
	 * within the bounding box.
	 * 
	 * @var ?bool
	 */
	protected ?bool $_bounded = null;
	
	/**
	 * Whether to include a breakdown of the address into elements.
	 * 
	 * @var ?bool
	 */
	protected ?bool $_addressDetails = null;
	
	/**
	 * If you are making large numbers of request please include a valid email
	 * address or alternatively include your email address as part of the
	 * User-Agent string. This information will be kept confidential and only
	 * used to contact you in the event of a problem, see Usage Policy for more
	 * details.
	 * 
	 * @var ?EmailAddressInterface
	 */
	protected ?EmailAddressInterface $_email = null;
	
	/**
	 * If you do not want certain openstreetmap objects to appear in the search
	 * result, give a comma separated list of the place_id's you want to skip.
	 * This can be used to broaden search results. For example, if a previous
	 * query only returned a few results, then including those here would cause
	 * the search to return other, less accurate, matches (if possible).
	 * 
	 * @var array<int, int>
	 */
	protected array $_excludePlaceIds = [];
	
	/**
	 * Limit the number of returned results. Default is 10.
	 * 
	 * @var ?int
	 */
	protected ?int $_limit = null;
	
	/**
	 * Sometimes you have several objects in OSM identifying the same place or
	 * object in reality. The simplest case is a street being split in many
	 * different OSM ways due to different characteristics. Nominatim will
	 * attempt to detect such duplicates and only return one match; this is
	 * controlled by the dedupe parameter which defaults to 1. Since the limit
	 * is, for reasons of efficiency, enforced before and not after
	 * de-duplicating, it is possible that de-duplicating leaves you with less
	 * results than requested.
	 * 
	 * @var ?bool
	 */
	protected ?bool $_dedupe = null;
	
	/**
	 * Output assorted developer debug information. Data on internals of
	 * nominatim "Search Loop" logic, and SQL queries. The output is (rough)
	 * HTML format. This overrides the specified machine readable format.
	 * 
	 * @var ?bool
	 */
	protected ?bool $_debug = null;
	
	/**
	 * Whether to output geometry of results in geojson format.
	 * 
	 * @var ?bool
	 */
	protected ?bool $_polygonGeojson = null;
	
	/**
	 * Whether to output geometry of results in kml format.
	 * 
	 * @var ?bool
	 */
	protected ?bool $_polygonKml = null;
	
	/**
	 * Whether to output geometry of results in svg format.
	 * 
	 * @var ?bool
	 */
	protected ?bool $_polygonSvg = null;
	
	/**
	 * Whether to output geometry of results as a WKT.
	 * 
	 * @var ?bool
	 */
	protected ?bool $_polygonText = null;
	
	/**
	 * Whether to include additional information in the result if available,
	 * e.g. wikipedia link, opening hours.
	 * 
	 * @var ?bool
	 */
	protected ?bool $_extraTags = null;
	
	/**
	 * Include a list of alternative names in the results. These may include
	 * language variants, references, operator and brand.
	 * 
	 * @var ?bool
	 */
	protected ?bool $_nameDetails = null;
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * Sets preferred language order for showing search results, overrides the
	 * value specified in the "Accept-Language" HTTP header. Either uses
	 * standard rfc2616 accept-language string or a simple comma separated list
	 * of language codes.
	 * 
	 * @param ?AcceptLanguageChainInterface $acceptLanguage
	 * @return ApiOrgOpenstreetmapNominatimRequestInterface
	 */
	public function setAcceptLanguage(?AcceptLanguageChainInterface $acceptLanguage) : ApiOrgOpenstreetmapNominatimRequestInterface
	{
		$this->_acceptLanguage = $acceptLanguage;
		
		return $this;
	}
	
	/**
	 * Gets preferred language order for showing search results, overrides the
	 * value specified in the "Accept-Language" HTTP header. Either uses
	 * standard rfc2616 accept-language string or a simple comma separated list
	 * of language codes.
	 * 
	 * @return ?AcceptLanguageChainInterface
	 */
	public function getAcceptLanguage() : ?AcceptLanguageChainInterface
	{
		return $this->_acceptLanguage;
	}
	
	/**
	 * Sets query string to search for. If this is set, the values for all the
	 * attributes {street, postal_code, city, county, state, country} are
	 * erased.
	 * 
	 * @param ?string $query
	 * @return ApiOrgOpenstreetmapNominatimRequestInterface
	 */
	public function setQuery(?string $query) : ApiOrgOpenstreetmapNominatimRequestInterface
	{
		$this->_query = $query;
		
		return $this;
	}
	
	/**
	 * Gets query string to search for. If this is set, the values for all the
	 * attributes {street, postal_code, city, county, state, country} are
	 * erased.
	 * 
	 * @return ?string
	 */
	public function getQuery() : ?string
	{
		return $this->_query;
	}
	
	/**
	 * Sets street number and street name.
	 * 
	 * @param ?string $street
	 * @return ApiOrgOpenstreetmapNominatimRequestInterface
	 */
	public function setStreet(?string $street) : ApiOrgOpenstreetmapNominatimRequestInterface
	{
		$this->_street = $street;
		
		return $this;
	}
	
	/**
	 * Gets street number and street name.
	 * 
	 * @return ?string
	 */
	public function getStreet() : ?string
	{
		return $this->_street;
	}
	
	/**
	 * Sets the postal code of the city.
	 * 
	 * @param ?string $postalCode
	 * @return ApiOrgOpenstreetmapNominatimRequestInterface
	 */
	public function setPostalCode(?string $postalCode) : ApiOrgOpenstreetmapNominatimRequestInterface
	{
		$this->_postalCode = $postalCode;
		
		return $this;
	}
	
	/**
	 * Gets the postal code of the city.
	 * 
	 * @return ?string
	 */
	public function getPostalCode() : ?string
	{
		return $this->_postalCode;
	}
	
	/**
	 * Sets the name of the city.
	 * 
	 * @param ?string $city
	 * @return ApiOrgOpenstreetmapNominatimRequestInterface
	 */
	public function setCity(?string $city) : ApiOrgOpenstreetmapNominatimRequestInterface
	{
		$this->_city = $city;
		
		return $this;
	}
	
	/**
	 * Gets the name of the city.
	 * 
	 * @return ?string
	 */
	public function getCity() : ?string
	{
		return $this->_city;
	}
	
	/**
	 * Sets the county of the query.
	 * 
	 * @param ?string $county
	 * @return ApiOrgOpenstreetmapNominatimRequestInterface
	 */
	public function setCounty(?string $county) : ApiOrgOpenstreetmapNominatimRequestInterface
	{
		$this->_county = $county;
		
		return $this;
	}
	
	/**
	 * Gets the county of the query.
	 * 
	 * @return ?string
	 */
	public function getCounty() : ?string
	{
		return $this->_county;
	}
	
	/**
	 * Sets the state of the query.
	 * 
	 * @param ?string $state
	 * @return ApiOrgOpenstreetmapNominatimRequestInterface
	 */
	public function setState(?string $state) : ApiOrgOpenstreetmapNominatimRequestInterface
	{
		$this->_state = $state;
		
		return $this;
	}
	
	/**
	 * Gets the state of the query.
	 * 
	 * @return ?string
	 */
	public function getState() : ?string
	{
		return $this->_state;
	}
	
	/**
	 * Sets the country of the query.
	 * 
	 * @param ?string $country
	 * @return ApiOrgOpenstreetmapNominatimRequestInterface
	 */
	public function setCountry(?string $country) : ApiOrgOpenstreetmapNominatimRequestInterface
	{
		$this->_country = $country;
		
		return $this;
	}
	
	/**
	 * Gets the country of the query.
	 * 
	 * @return ?string
	 */
	public function getCountry() : ?string
	{
		return $this->_country;
	}
	
	/**
	 * Sets limit search results to a specific country (or a list of
	 * countries). <countrycode> should be the ISO 3166-1alpha2 code, e.g. gb
	 * for the United Kingdom, de for Germany, etc.
	 * 
	 * @param array<int, string> $countryCodes
	 * @return ApiOrgOpenstreetmapNominatimRequestInterface
	 */
	public function setCountryCodes(array $countryCodes) : ApiOrgOpenstreetmapNominatimRequestInterface
	{
		$this->_countryCodes = $countryCodes;
		
		return $this;
	}
	
	/**
	 * Gets limit search results to a specific country (or a list of
	 * countries). <countrycode> should be the ISO 3166-1alpha2 code, e.g. gb
	 * for the United Kingdom, de for Germany, etc.
	 * 
	 * @return array<int, string>
	 */
	public function getCountryCodes() : array
	{
		return $this->_countryCodes;
	}
	
	/**
	 * Sets the preferred area to find search results. Any two corner points of
	 * the box are accepted in any order as long as they span a real box.
	 * 
	 * @param array<int, int> $viewbox
	 * @return ApiOrgOpenstreetmapNominatimRequestInterface
	 */
	public function setViewbox(array $viewbox) : ApiOrgOpenstreetmapNominatimRequestInterface
	{
		$this->_viewbox = $viewbox;
		
		return $this;
	}
	
	/**
	 * Gets the preferred area to find search results. Any two corner points of
	 * the box are accepted in any order as long as they span a real box.
	 * 
	 * @return array<int, int>
	 */
	public function getViewbox() : array
	{
		return $this->_viewbox;
	}
	
	/**
	 * Sets restrict the results to only items contained with the viewbox (see
	 * above).
	 * Restricting the results to the bounding box also enables searching by
	 * amenity only. For example a search query of just "[pub]" would normally
	 * be rejected but with bounded=1 will result in a list of items matching
	 * within the bounding box.
	 * 
	 * @param ?bool $bounded
	 * @return ApiOrgOpenstreetmapNominatimRequestInterface
	 */
	public function setBounded(?bool $bounded) : ApiOrgOpenstreetmapNominatimRequestInterface
	{
		$this->_bounded = $bounded;
		
		return $this;
	}
	
	/**
	 * Gets restrict the results to only items contained with the viewbox (see
	 * above).
	 * Restricting the results to the bounding box also enables searching by
	 * amenity only. For example a search query of just "[pub]" would normally
	 * be rejected but with bounded=1 will result in a list of items matching
	 * within the bounding box.
	 * 
	 * @return ?bool
	 */
	public function hasBounded() : ?bool
	{
		return $this->_bounded;
	}
	
	/**
	 * Sets whether to include a breakdown of the address into elements.
	 * 
	 * @param ?bool $addressDetails
	 * @return ApiOrgOpenstreetmapNominatimRequestInterface
	 */
	public function setAddressDetails(?bool $addressDetails) : ApiOrgOpenstreetmapNominatimRequestInterface
	{
		$this->_addressDetails = $addressDetails;
		
		return $this;
	}
	
	/**
	 * Gets whether to include a breakdown of the address into elements.
	 * 
	 * @return ?bool
	 */
	public function hasAddressDetails() : ?bool
	{
		return $this->_addressDetails;
	}
	
	/**
	 * Sets if you are making large numbers of request please include a valid
	 * email address or alternatively include your email address as part of the
	 * User-Agent string. This information will be kept confidential and only
	 * used to contact you in the event of a problem, see Usage Policy for more
	 * details.
	 * 
	 * @param ?EmailAddressInterface $email
	 * @return ApiOrgOpenstreetmapNominatimRequestInterface
	 */
	public function setEmail(?EmailAddressInterface $email) : ApiOrgOpenstreetmapNominatimRequestInterface
	{
		$this->_email = $email;
		
		return $this;
	}
	
	/**
	 * Gets if you are making large numbers of request please include a valid
	 * email address or alternatively include your email address as part of the
	 * User-Agent string. This information will be kept confidential and only
	 * used to contact you in the event of a problem, see Usage Policy for more
	 * details.
	 * 
	 * @return ?EmailAddressInterface
	 */
	public function getEmail() : ?EmailAddressInterface
	{
		return $this->_email;
	}
	
	/**
	 * Sets if you do not want certain openstreetmap objects to appear in the
	 * search result, give a comma separated list of the place_id's you want to
	 * skip.
	 * This can be used to broaden search results. For example, if a previous
	 * query only returned a few results, then including those here would cause
	 * the search to return other, less accurate, matches (if possible).
	 * 
	 * @param array<int, int> $excludePlaceIds
	 * @return ApiOrgOpenstreetmapNominatimRequestInterface
	 */
	public function setExcludePlaceIds(array $excludePlaceIds) : ApiOrgOpenstreetmapNominatimRequestInterface
	{
		$this->_excludePlaceIds = $excludePlaceIds;
		
		return $this;
	}
	
	/**
	 * Gets if you do not want certain openstreetmap objects to appear in the
	 * search result, give a comma separated list of the place_id's you want to
	 * skip.
	 * This can be used to broaden search results. For example, if a previous
	 * query only returned a few results, then including those here would cause
	 * the search to return other, less accurate, matches (if possible).
	 * 
	 * @return array<int, int>
	 */
	public function getExcludePlaceIds() : array
	{
		return $this->_excludePlaceIds;
	}
	
	/**
	 * Sets limit the number of returned results. Default is 10.
	 * 
	 * @param ?int $limit
	 * @return ApiOrgOpenstreetmapNominatimRequestInterface
	 */
	public function setLimit(?int $limit) : ApiOrgOpenstreetmapNominatimRequestInterface
	{
		$this->_limit = $limit;
		
		return $this;
	}
	
	/**
	 * Gets limit the number of returned results. Default is 10.
	 * 
	 * @return ?int
	 */
	public function getLimit() : ?int
	{
		return $this->_limit;
	}
	
	/**
	 * Sets sometimes you have several objects in OSM identifying the same
	 * place or object in reality. The simplest case is a street being split in
	 * many different OSM ways due to different characteristics. Nominatim will
	 * attempt to detect such duplicates and only return one match; this is
	 * controlled by the dedupe parameter which defaults to 1. Since the limit
	 * is, for reasons of efficiency, enforced before and not after
	 * de-duplicating, it is possible that de-duplicating leaves you with less
	 * results than requested.
	 * 
	 * @param ?bool $dedupe
	 * @return ApiOrgOpenstreetmapNominatimRequestInterface
	 */
	public function setDedupe(?bool $dedupe) : ApiOrgOpenstreetmapNominatimRequestInterface
	{
		$this->_dedupe = $dedupe;
		
		return $this;
	}
	
	/**
	 * Gets sometimes you have several objects in OSM identifying the same
	 * place or object in reality. The simplest case is a street being split in
	 * many different OSM ways due to different characteristics. Nominatim will
	 * attempt to detect such duplicates and only return one match; this is
	 * controlled by the dedupe parameter which defaults to 1. Since the limit
	 * is, for reasons of efficiency, enforced before and not after
	 * de-duplicating, it is possible that de-duplicating leaves you with less
	 * results than requested.
	 * 
	 * @return ?bool
	 */
	public function hasDedupe() : ?bool
	{
		return $this->_dedupe;
	}
	
	/**
	 * Sets output assorted developer debug information. Data on internals of
	 * nominatim "Search Loop" logic, and SQL queries. The output is (rough)
	 * HTML format. This overrides the specified machine readable format.
	 * 
	 * @param ?bool $debug
	 * @return ApiOrgOpenstreetmapNominatimRequestInterface
	 */
	public function setDebug(?bool $debug) : ApiOrgOpenstreetmapNominatimRequestInterface
	{
		$this->_debug = $debug;
		
		return $this;
	}
	
	/**
	 * Gets output assorted developer debug information. Data on internals of
	 * nominatim "Search Loop" logic, and SQL queries. The output is (rough)
	 * HTML format. This overrides the specified machine readable format.
	 * 
	 * @return ?bool
	 */
	public function hasDebug() : ?bool
	{
		return $this->_debug;
	}
	
	/**
	 * Sets whether to output geometry of results in geojson format.
	 * 
	 * @param ?bool $polygonGeojson
	 * @return ApiOrgOpenstreetmapNominatimRequestInterface
	 */
	public function setPolygonGeojson(?bool $polygonGeojson) : ApiOrgOpenstreetmapNominatimRequestInterface
	{
		$this->_polygonGeojson = $polygonGeojson;
		
		return $this;
	}
	
	/**
	 * Gets whether to output geometry of results in geojson format.
	 * 
	 * @return ?bool
	 */
	public function hasPolygonGeojson() : ?bool
	{
		return $this->_polygonGeojson;
	}
	
	/**
	 * Sets whether to output geometry of results in kml format.
	 * 
	 * @param ?bool $polygonKml
	 * @return ApiOrgOpenstreetmapNominatimRequestInterface
	 */
	public function setPolygonKml(?bool $polygonKml) : ApiOrgOpenstreetmapNominatimRequestInterface
	{
		$this->_polygonKml = $polygonKml;
		
		return $this;
	}
	
	/**
	 * Gets whether to output geometry of results in kml format.
	 * 
	 * @return ?bool
	 */
	public function hasPolygonKml() : ?bool
	{
		return $this->_polygonKml;
	}
	
	/**
	 * Sets whether to output geometry of results in svg format.
	 * 
	 * @param ?bool $polygonSvg
	 * @return ApiOrgOpenstreetmapNominatimRequestInterface
	 */
	public function setPolygonSvg(?bool $polygonSvg) : ApiOrgOpenstreetmapNominatimRequestInterface
	{
		$this->_polygonSvg = $polygonSvg;
		
		return $this;
	}
	
	/**
	 * Gets whether to output geometry of results in svg format.
	 * 
	 * @return ?bool
	 */
	public function hasPolygonSvg() : ?bool
	{
		return $this->_polygonSvg;
	}
	
	/**
	 * Sets whether to output geometry of results as a WKT.
	 * 
	 * @param ?bool $polygonText
	 * @return ApiOrgOpenstreetmapNominatimRequestInterface
	 */
	public function setPolygonText(?bool $polygonText) : ApiOrgOpenstreetmapNominatimRequestInterface
	{
		$this->_polygonText = $polygonText;
		
		return $this;
	}
	
	/**
	 * Gets whether to output geometry of results as a WKT.
	 * 
	 * @return ?bool
	 */
	public function hasPolygonText() : ?bool
	{
		return $this->_polygonText;
	}
	
	/**
	 * Sets whether to include additional information in the result if
	 * available, e.g. wikipedia link, opening hours.
	 * 
	 * @param ?bool $extraTags
	 * @return ApiOrgOpenstreetmapNominatimRequestInterface
	 */
	public function setExtraTags(?bool $extraTags) : ApiOrgOpenstreetmapNominatimRequestInterface
	{
		$this->_extraTags = $extraTags;
		
		return $this;
	}
	
	/**
	 * Gets whether to include additional information in the result if
	 * available, e.g. wikipedia link, opening hours.
	 * 
	 * @return ?bool
	 */
	public function hasExtraTags() : ?bool
	{
		return $this->_extraTags;
	}
	
	/**
	 * Sets include a list of alternative names in the results. These may
	 * include language variants, references, operator and brand.
	 * 
	 * @param ?bool $nameDetails
	 * @return ApiOrgOpenstreetmapNominatimRequestInterface
	 */
	public function setNameDetails(?bool $nameDetails) : ApiOrgOpenstreetmapNominatimRequestInterface
	{
		$this->_nameDetails = $nameDetails;
		
		return $this;
	}
	
	/**
	 * Gets include a list of alternative names in the results. These may
	 * include language variants, references, operator and brand.
	 * 
	 * @return ?bool
	 */
	public function hasNameDetails() : ?bool
	{
		return $this->_nameDetails;
	}
	
}
