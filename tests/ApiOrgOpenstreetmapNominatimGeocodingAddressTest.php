<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-org-openstreetmap-nominatim-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Osm\Test;

use PhpExtended\Osm\ApiOrgOpenstreetmapNominatimGeocodingAddress;
use PHPUnit\Framework\TestCase;

/**
 * ApiOrgOpenstreetmapNominatimGeocodingAddressTest test file.
 * 
 * /!\ This file was generated automatically from the json-schema.json file.
 * /!\ Do not edit by hand or the modifications will be erased.
 * @generator PhpExtended\JsonSchema\Php74TestMetadata
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Osm\ApiOrgOpenstreetmapNominatimGeocodingAddress
 * @internal
 * @small
 */
class ApiOrgOpenstreetmapNominatimGeocodingAddressTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var ApiOrgOpenstreetmapNominatimGeocodingAddress
	 */
	protected ApiOrgOpenstreetmapNominatimGeocodingAddress $_object;
	
	public function testToString() : void
	{
		$this->assertEquals(\get_class($this->_object).'@'.\spl_object_hash($this->_object), $this->_object->__toString());
	}
	
	public function testGetAmenity() : void
	{
		$this->assertNull($this->_object->getAmenity());
		$expected = 'qsdfghjklm';
		$this->_object->setAmenity($expected);
		$this->assertEquals($expected, $this->_object->getAmenity());
	}
	
	public function testGetBorough() : void
	{
		$this->assertNull($this->_object->getBorough());
		$expected = 'qsdfghjklm';
		$this->_object->setBorough($expected);
		$this->assertEquals($expected, $this->_object->getBorough());
	}
	
	public function testGetHouseNumber() : void
	{
		$this->assertNull($this->_object->getHouseNumber());
		$expected = 'qsdfghjklm';
		$this->_object->setHouseNumber($expected);
		$this->assertEquals($expected, $this->_object->getHouseNumber());
	}
	
	public function testGetRoad() : void
	{
		$this->assertNull($this->_object->getRoad());
		$expected = 'qsdfghjklm';
		$this->_object->setRoad($expected);
		$this->assertEquals($expected, $this->_object->getRoad());
	}
	
	public function testGetQuarter() : void
	{
		$this->assertNull($this->_object->getQuarter());
		$expected = 'qsdfghjklm';
		$this->_object->setQuarter($expected);
		$this->assertEquals($expected, $this->_object->getQuarter());
	}
	
	public function testGetNeighbourhood() : void
	{
		$this->assertNull($this->_object->getNeighbourhood());
		$expected = 'qsdfghjklm';
		$this->_object->setNeighbourhood($expected);
		$this->assertEquals($expected, $this->_object->getNeighbourhood());
	}
	
	public function testGetDistrict() : void
	{
		$this->assertNull($this->_object->getDistrict());
		$expected = 'qsdfghjklm';
		$this->_object->setDistrict($expected);
		$this->assertEquals($expected, $this->_object->getDistrict());
	}
	
	public function testGetSuburb() : void
	{
		$this->assertNull($this->_object->getSuburb());
		$expected = 'qsdfghjklm';
		$this->_object->setSuburb($expected);
		$this->assertEquals($expected, $this->_object->getSuburb());
	}
	
	public function testGetCounty() : void
	{
		$this->assertNull($this->_object->getCounty());
		$expected = 'qsdfghjklm';
		$this->_object->setCounty($expected);
		$this->assertEquals($expected, $this->_object->getCounty());
	}
	
	public function testGetCity() : void
	{
		$this->assertNull($this->_object->getCity());
		$expected = 'qsdfghjklm';
		$this->_object->setCity($expected);
		$this->assertEquals($expected, $this->_object->getCity());
	}
	
	public function testGetCityBlock() : void
	{
		$this->assertNull($this->_object->getCityBlock());
		$expected = 'qsdfghjklm';
		$this->_object->setCityBlock($expected);
		$this->assertEquals($expected, $this->_object->getCityBlock());
	}
	
	public function testGetState() : void
	{
		$this->assertNull($this->_object->getState());
		$expected = 'qsdfghjklm';
		$this->_object->setState($expected);
		$this->assertEquals($expected, $this->_object->getState());
	}
	
	public function testGetPostCode() : void
	{
		$this->assertNull($this->_object->getPostCode());
		$expected = 'qsdfghjklm';
		$this->_object->setPostCode($expected);
		$this->assertEquals($expected, $this->_object->getPostCode());
	}
	
	public function testGetCountry() : void
	{
		$this->assertNull($this->_object->getCountry());
		$expected = 'qsdfghjklm';
		$this->_object->setCountry($expected);
		$this->assertEquals($expected, $this->_object->getCountry());
	}
	
	public function testGetCountryCode() : void
	{
		$this->assertNull($this->_object->getCountryCode());
		$expected = 'qsdfghjklm';
		$this->_object->setCountryCode($expected);
		$this->assertEquals($expected, $this->_object->getCountryCode());
	}
	
	public function testGetIso31662lvl4() : void
	{
		$this->assertNull($this->_object->getIso31662lvl4());
		$expected = 'qsdfghjklm';
		$this->_object->setIso31662lvl4($expected);
		$this->assertEquals($expected, $this->_object->getIso31662lvl4());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new ApiOrgOpenstreetmapNominatimGeocodingAddress();
	}
	
}
