<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-org-openstreetmap-nominatim-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\HttpMessage\Response;
use PhpExtended\HttpMessage\StringStream;
use PhpExtended\Osm\ApiOrgOpenstreetmapNominatimEndpoint;
use PhpExtended\Osm\ApiOrgOpenstreetmapNominatimGeocodingResult;
use PhpExtended\Osm\ApiOrgOpenstreetmapNominatimRequest;
use PhpExtended\Osm\ApiOrgOpenstreetmapNominatimReverseRequest;
use PHPUnit\Framework\TestCase;
use Psr\Http\Client\ClientInterface;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;

/**
 * ApiOrgOpenstreetmapNominatimEndpointTest class file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Osm\ApiOrgOpenstreetmapNominatimEndpoint
 *
 * @internal
 *
 * @small
 */
class ApiOrgOpenstreetmapNominatimEndpointTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var ApiOrgOpenstreetmapNominatimEndpoint
	 */
	protected ApiOrgOpenstreetmapNominatimEndpoint $_object;
	
// 	public function testDirectRequest() : void
// 	{
// 		$request = new ApiOrgOpenstreetmapNominatimRequest();
// 		$request->city = 'New York';
		
// 		$response = $this->_object->searchGeocode($request);
		
// 		foreach($response as $result)
// 		{
// 			$this->assertInstanceOf(ApiOrgOpenstreetmapNominatimGeocodingResult::class, $result);
// 		}
// 	}
	
// 	public function testDirectGeojson() : void
// 	{
// 		$request = new ApiOrgOpenstreetmapNominatimRequest();
// 		$request->city = 'New York';
// 		$request->polygonGeojson = true;
		
// 		$response = $this->_object->searchGeocode($request);
		
// 		foreach($response as $result)
// 		{
// 			$this->assertInstanceOf(ApiOrgOpenstreetmapNominatimGeocodingResult::class, $result);
// 		}
// 	}
	
// 	public function testReverseRequest() : void
// 	{
// 		$request = new ApiOrgOpenstreetmapNominatimReverseRequest();
// 		$request->latitude = 40.7127281;
// 		$request->longitude = -74.0060152;
		
// 		$response = $this->_object->searchReverseGeocode($request);
		
// 		$this->assertInstanceOf(ApiOrgOpenstreetmapNominatimGeocodingResult::class, $response);
// 	}
	
// 	public function testReverseGeojson() : void
// 	{
// 		$request = new ApiOrgOpenstreetmapNominatimReverseRequest();
// 		$request->latitude = 40.7127281;
// 		$request->longitude = -74.0060152;
// 		$request->polygonGeojson = true;
		
// 		$response = $this->_object->searchReverseGeocode($request);
		
// 		$this->assertInstanceOf(ApiOrgOpenstreetmapNominatimGeocodingResult::class, $response);
// 	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$client = new class() implements ClientInterface
		{
			public function sendRequest(RequestInterface $request) : ResponseInterface
			{
				\sleep(1);
// 				\var_dump($request->getUri()->__toString());
				$options = ['http' => ['user_agent' => 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:95.0) Gecko/20100101 Firefox/95.0']];
				$data = \file_get_contents($request->getUri()->__toString(), false, \stream_context_create($options));
				$body = new StringStream($data);
				
				return (new Response())->withBody($body);
			}
		};
		
		$this->_object = new ApiOrgOpenstreetmapNominatimEndpoint($client);
	}
	
}
