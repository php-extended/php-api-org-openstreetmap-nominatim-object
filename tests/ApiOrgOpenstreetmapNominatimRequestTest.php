<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-org-openstreetmap-nominatim-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Osm\Test;

use PhpExtended\Email\EmailAddressParser;
use PhpExtended\HttpClient\AcceptLanguageChainParser;
use PhpExtended\Osm\ApiOrgOpenstreetmapNominatimRequest;
use PHPUnit\Framework\TestCase;

/**
 * ApiOrgOpenstreetmapNominatimRequestTest test file.
 * 
 * /!\ This file was generated automatically from the json-schema.json file.
 * /!\ Do not edit by hand or the modifications will be erased.
 * @generator PhpExtended\JsonSchema\Php74TestMetadata
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Osm\ApiOrgOpenstreetmapNominatimRequest
 * @internal
 * @small
 */
class ApiOrgOpenstreetmapNominatimRequestTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var ApiOrgOpenstreetmapNominatimRequest
	 */
	protected ApiOrgOpenstreetmapNominatimRequest $_object;
	
	public function testToString() : void
	{
		$this->assertEquals(\get_class($this->_object).'@'.\spl_object_hash($this->_object), $this->_object->__toString());
	}
	
	public function testGetAcceptLanguage() : void
	{
		$this->assertNull($this->_object->getAcceptLanguage());
		$expected = (new AcceptLanguageChainParser())->parse('fr-FR,en-US;q=0.8,en;q=0.5');
		$this->_object->setAcceptLanguage($expected);
		$this->assertEquals($expected, $this->_object->getAcceptLanguage());
	}
	
	public function testGetQuery() : void
	{
		$this->assertNull($this->_object->getQuery());
		$expected = 'qsdfghjklm';
		$this->_object->setQuery($expected);
		$this->assertEquals($expected, $this->_object->getQuery());
	}
	
	public function testGetStreet() : void
	{
		$this->assertNull($this->_object->getStreet());
		$expected = 'qsdfghjklm';
		$this->_object->setStreet($expected);
		$this->assertEquals($expected, $this->_object->getStreet());
	}
	
	public function testGetPostalCode() : void
	{
		$this->assertNull($this->_object->getPostalCode());
		$expected = 'qsdfghjklm';
		$this->_object->setPostalCode($expected);
		$this->assertEquals($expected, $this->_object->getPostalCode());
	}
	
	public function testGetCity() : void
	{
		$this->assertNull($this->_object->getCity());
		$expected = 'qsdfghjklm';
		$this->_object->setCity($expected);
		$this->assertEquals($expected, $this->_object->getCity());
	}
	
	public function testGetCounty() : void
	{
		$this->assertNull($this->_object->getCounty());
		$expected = 'qsdfghjklm';
		$this->_object->setCounty($expected);
		$this->assertEquals($expected, $this->_object->getCounty());
	}
	
	public function testGetState() : void
	{
		$this->assertNull($this->_object->getState());
		$expected = 'qsdfghjklm';
		$this->_object->setState($expected);
		$this->assertEquals($expected, $this->_object->getState());
	}
	
	public function testGetCountry() : void
	{
		$this->assertNull($this->_object->getCountry());
		$expected = 'qsdfghjklm';
		$this->_object->setCountry($expected);
		$this->assertEquals($expected, $this->_object->getCountry());
	}
	
	public function testGetCountryCodes() : void
	{
		$this->assertEquals([], $this->_object->getCountryCodes());
		$expected = ['qsdfghjklm', 'qsdfghjklm'];
		$this->_object->setCountryCodes($expected);
		$this->assertEquals($expected, $this->_object->getCountryCodes());
	}
	
	public function testGetViewbox() : void
	{
		$this->assertEquals([], $this->_object->getViewbox());
		$expected = [25, 25];
		$this->_object->setViewbox($expected);
		$this->assertEquals($expected, $this->_object->getViewbox());
	}
	
	public function testHasBounded() : void
	{
		$this->assertNull($this->_object->hasBounded());
		$expected = true;
		$this->_object->setBounded($expected);
		$this->assertTrue($this->_object->hasBounded());
	}
	
	public function testHasAddressDetails() : void
	{
		$this->assertNull($this->_object->hasAddressDetails());
		$expected = true;
		$this->_object->setAddressDetails($expected);
		$this->assertTrue($this->_object->hasAddressDetails());
	}
	
	public function testGetEmail() : void
	{
		$this->assertNull($this->_object->getEmail());
		$expected = (new EmailAddressParser())->parse('admin@example.com');
		$this->_object->setEmail($expected);
		$this->assertEquals($expected, $this->_object->getEmail());
	}
	
	public function testGetExcludePlaceIds() : void
	{
		$this->assertEquals([], $this->_object->getExcludePlaceIds());
		$expected = [25, 25];
		$this->_object->setExcludePlaceIds($expected);
		$this->assertEquals($expected, $this->_object->getExcludePlaceIds());
	}
	
	public function testGetLimit() : void
	{
		$this->assertNull($this->_object->getLimit());
		$expected = 25;
		$this->_object->setLimit($expected);
		$this->assertEquals($expected, $this->_object->getLimit());
	}
	
	public function testHasDedupe() : void
	{
		$this->assertNull($this->_object->hasDedupe());
		$expected = true;
		$this->_object->setDedupe($expected);
		$this->assertTrue($this->_object->hasDedupe());
	}
	
	public function testHasDebug() : void
	{
		$this->assertNull($this->_object->hasDebug());
		$expected = true;
		$this->_object->setDebug($expected);
		$this->assertTrue($this->_object->hasDebug());
	}
	
	public function testHasPolygonGeojson() : void
	{
		$this->assertNull($this->_object->hasPolygonGeojson());
		$expected = true;
		$this->_object->setPolygonGeojson($expected);
		$this->assertTrue($this->_object->hasPolygonGeojson());
	}
	
	public function testHasPolygonKml() : void
	{
		$this->assertNull($this->_object->hasPolygonKml());
		$expected = true;
		$this->_object->setPolygonKml($expected);
		$this->assertTrue($this->_object->hasPolygonKml());
	}
	
	public function testHasPolygonSvg() : void
	{
		$this->assertNull($this->_object->hasPolygonSvg());
		$expected = true;
		$this->_object->setPolygonSvg($expected);
		$this->assertTrue($this->_object->hasPolygonSvg());
	}
	
	public function testHasPolygonText() : void
	{
		$this->assertNull($this->_object->hasPolygonText());
		$expected = true;
		$this->_object->setPolygonText($expected);
		$this->assertTrue($this->_object->hasPolygonText());
	}
	
	public function testHasExtraTags() : void
	{
		$this->assertNull($this->_object->hasExtraTags());
		$expected = true;
		$this->_object->setExtraTags($expected);
		$this->assertTrue($this->_object->hasExtraTags());
	}
	
	public function testHasNameDetails() : void
	{
		$this->assertNull($this->_object->hasNameDetails());
		$expected = true;
		$this->_object->setNameDetails($expected);
		$this->assertTrue($this->_object->hasNameDetails());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new ApiOrgOpenstreetmapNominatimRequest();
	}
	
}
