<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-org-openstreetmap-nominatim-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Osm\Test;

use PhpExtended\Osm\ApiOrgOpenstreetmapNominatimNameDetails;
use PHPUnit\Framework\TestCase;

/**
 * ApiOrgOpenstreetmapNominatimNameDetailsTest test file.
 * 
 * /!\ This file was generated automatically from the json-schema.json file.
 * /!\ Do not edit by hand or the modifications will be erased.
 * @generator PhpExtended\JsonSchema\Php74TestMetadata
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Osm\ApiOrgOpenstreetmapNominatimNameDetails
 * @internal
 * @small
 */
class ApiOrgOpenstreetmapNominatimNameDetailsTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var ApiOrgOpenstreetmapNominatimNameDetails
	 */
	protected ApiOrgOpenstreetmapNominatimNameDetails $_object;
	
	public function testToString() : void
	{
		$this->assertEquals(\get_class($this->_object).'@'.\spl_object_hash($this->_object), $this->_object->__toString());
	}
	
	public function testGetName() : void
	{
		$this->assertEquals('azertyuiop', $this->_object->getName());
		$expected = 'qsdfghjklm';
		$this->_object->setName($expected);
		$this->assertEquals($expected, $this->_object->getName());
	}
	
	public function testGetNames() : void
	{
		$this->assertEquals(['key' => 'azertyuiop'], $this->_object->getNames());
		$expected = ['key1' => 'qsdfghjklm', 'key2' => 'qsdfghjklm'];
		$this->_object->setNames($expected);
		$this->assertEquals($expected, $this->_object->getNames());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new ApiOrgOpenstreetmapNominatimNameDetails('azertyuiop', ['key' => 'azertyuiop']);
	}
	
}
