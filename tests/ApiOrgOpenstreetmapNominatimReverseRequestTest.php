<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-org-openstreetmap-nominatim-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Osm\Test;

use PhpExtended\Email\EmailAddressParser;
use PhpExtended\HttpClient\AcceptLanguageChainParser;
use PhpExtended\Osm\ApiOrgOpenstreetmapNominatimReverseRequest;
use PHPUnit\Framework\TestCase;

/**
 * ApiOrgOpenstreetmapNominatimReverseRequestTest test file.
 * 
 * /!\ This file was generated automatically from the json-schema.json file.
 * /!\ Do not edit by hand or the modifications will be erased.
 * @generator PhpExtended\JsonSchema\Php74TestMetadata
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Osm\ApiOrgOpenstreetmapNominatimReverseRequest
 * @internal
 * @small
 */
class ApiOrgOpenstreetmapNominatimReverseRequestTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var ApiOrgOpenstreetmapNominatimReverseRequest
	 */
	protected ApiOrgOpenstreetmapNominatimReverseRequest $_object;
	
	public function testToString() : void
	{
		$this->assertEquals(\get_class($this->_object).'@'.\spl_object_hash($this->_object), $this->_object->__toString());
	}
	
	public function testGetAcceptLanguage() : void
	{
		$this->assertNull($this->_object->getAcceptLanguage());
		$expected = (new AcceptLanguageChainParser())->parse('fr-FR,en-US;q=0.8,en;q=0.5');
		$this->_object->setAcceptLanguage($expected);
		$this->assertEquals($expected, $this->_object->getAcceptLanguage());
	}
	
	public function testGetOsmType() : void
	{
		$this->assertNull($this->_object->getOsmType());
		$expected = 'qsdfghjklm';
		$this->_object->setOsmType($expected);
		$this->assertEquals($expected, $this->_object->getOsmType());
	}
	
	public function testGetOsmId() : void
	{
		$this->assertNull($this->_object->getOsmId());
		$expected = 'qsdfghjklm';
		$this->_object->setOsmId($expected);
		$this->assertEquals($expected, $this->_object->getOsmId());
	}
	
	public function testGetLatitude() : void
	{
		$this->assertNull($this->_object->getLatitude());
		$expected = 15.2;
		$this->_object->setLatitude($expected);
		$this->assertEquals($expected, $this->_object->getLatitude());
	}
	
	public function testGetLongitude() : void
	{
		$this->assertNull($this->_object->getLongitude());
		$expected = 15.2;
		$this->_object->setLongitude($expected);
		$this->assertEquals($expected, $this->_object->getLongitude());
	}
	
	public function testGetZoom() : void
	{
		$this->assertNull($this->_object->getZoom());
		$expected = 25;
		$this->_object->setZoom($expected);
		$this->assertEquals($expected, $this->_object->getZoom());
	}
	
	public function testHasAddressDetails() : void
	{
		$this->assertNull($this->_object->hasAddressDetails());
		$expected = true;
		$this->_object->setAddressDetails($expected);
		$this->assertTrue($this->_object->hasAddressDetails());
	}
	
	public function testGetEmail() : void
	{
		$this->assertNull($this->_object->getEmail());
		$expected = (new EmailAddressParser())->parse('admin@example.com');
		$this->_object->setEmail($expected);
		$this->assertEquals($expected, $this->_object->getEmail());
	}
	
	public function testHasPolygonGeojson() : void
	{
		$this->assertNull($this->_object->hasPolygonGeojson());
		$expected = true;
		$this->_object->setPolygonGeojson($expected);
		$this->assertTrue($this->_object->hasPolygonGeojson());
	}
	
	public function testHasPolygonKml() : void
	{
		$this->assertNull($this->_object->hasPolygonKml());
		$expected = true;
		$this->_object->setPolygonKml($expected);
		$this->assertTrue($this->_object->hasPolygonKml());
	}
	
	public function testHasPolygonSvg() : void
	{
		$this->assertNull($this->_object->hasPolygonSvg());
		$expected = true;
		$this->_object->setPolygonSvg($expected);
		$this->assertTrue($this->_object->hasPolygonSvg());
	}
	
	public function testHasPolygonText() : void
	{
		$this->assertNull($this->_object->hasPolygonText());
		$expected = true;
		$this->_object->setPolygonText($expected);
		$this->assertTrue($this->_object->hasPolygonText());
	}
	
	public function testHasExtraTags() : void
	{
		$this->assertNull($this->_object->hasExtraTags());
		$expected = true;
		$this->_object->setExtraTags($expected);
		$this->assertTrue($this->_object->hasExtraTags());
	}
	
	public function testHasNameDetails() : void
	{
		$this->assertNull($this->_object->hasNameDetails());
		$expected = true;
		$this->_object->setNameDetails($expected);
		$this->assertTrue($this->_object->hasNameDetails());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new ApiOrgOpenstreetmapNominatimReverseRequest();
	}
	
}
