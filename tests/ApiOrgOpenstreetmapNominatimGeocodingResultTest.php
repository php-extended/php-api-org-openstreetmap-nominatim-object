<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-org-openstreetmap-nominatim-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Osm\Test;

use PhpExtended\GeoJson\GeoJsonBoundingBox;
use PhpExtended\GeoJson\GeoJsonGeometryInterface;
use PhpExtended\Osm\ApiOrgOpenstreetmapNominatimGeocodingAddress;
use PhpExtended\Osm\ApiOrgOpenstreetmapNominatimGeocodingResult;
use PhpExtended\Osm\ApiOrgOpenstreetmapNominatimNameDetails;
use PHPUnit\Framework\TestCase;

/**
 * ApiOrgOpenstreetmapNominatimGeocodingResultTest test file.
 * 
 * /!\ This file was generated automatically from the json-schema.json file.
 * /!\ Do not edit by hand or the modifications will be erased.
 * @generator PhpExtended\JsonSchema\Php74TestMetadata
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Osm\ApiOrgOpenstreetmapNominatimGeocodingResult
 * @internal
 * @small
 */
class ApiOrgOpenstreetmapNominatimGeocodingResultTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var ApiOrgOpenstreetmapNominatimGeocodingResult
	 */
	protected ApiOrgOpenstreetmapNominatimGeocodingResult $_object;
	
	public function testToString() : void
	{
		$this->assertEquals(\get_class($this->_object).'@'.\spl_object_hash($this->_object), $this->_object->__toString());
	}
	
	public function testGetAddress() : void
	{
		$this->assertNull($this->_object->getAddress());
		$expected = $this->getMockBuilder(ApiOrgOpenstreetmapNominatimGeocodingAddress::class)->disableOriginalConstructor()->getMock();
		$this->_object->setAddress($expected);
		$this->assertEquals($expected, $this->_object->getAddress());
	}
	
	public function testGetBoundingBox() : void
	{
		$this->assertNull($this->_object->getBoundingBox());
		$expected = $this->getMockBuilder(GeoJsonBoundingBox::class)->disableOriginalConstructor()->getMock();
		$this->_object->setBoundingBox($expected);
		$this->assertEquals($expected, $this->_object->getBoundingBox());
	}
	
	public function testGetClass() : void
	{
		$this->assertEquals('azertyuiop', $this->_object->getClass());
		$expected = 'qsdfghjklm';
		$this->_object->setClass($expected);
		$this->assertEquals($expected, $this->_object->getClass());
	}
	
	public function testGetDisplayName() : void
	{
		$this->assertEquals('azertyuiop', $this->_object->getDisplayName());
		$expected = 'qsdfghjklm';
		$this->_object->setDisplayName($expected);
		$this->assertEquals($expected, $this->_object->getDisplayName());
	}
	
	public function testGetImportance() : void
	{
		$this->assertEquals('azertyuiop', $this->_object->getImportance());
		$expected = 'qsdfghjklm';
		$this->_object->setImportance($expected);
		$this->assertEquals($expected, $this->_object->getImportance());
	}
	
	public function testGetLatitude() : void
	{
		$this->assertEquals('azertyuiop', $this->_object->getLatitude());
		$expected = 'qsdfghjklm';
		$this->_object->setLatitude($expected);
		$this->assertEquals($expected, $this->_object->getLatitude());
	}
	
	public function testGetLicence() : void
	{
		$this->assertEquals('azertyuiop', $this->_object->getLicence());
		$expected = 'qsdfghjklm';
		$this->_object->setLicence($expected);
		$this->assertEquals($expected, $this->_object->getLicence());
	}
	
	public function testGetLongitude() : void
	{
		$this->assertEquals('azertyuiop', $this->_object->getLongitude());
		$expected = 'qsdfghjklm';
		$this->_object->setLongitude($expected);
		$this->assertEquals($expected, $this->_object->getLongitude());
	}
	
	public function testGetOsmId() : void
	{
		$this->assertEquals('azertyuiop', $this->_object->getOsmId());
		$expected = 'qsdfghjklm';
		$this->_object->setOsmId($expected);
		$this->assertEquals($expected, $this->_object->getOsmId());
	}
	
	public function testGetOsmType() : void
	{
		$this->assertEquals('azertyuiop', $this->_object->getOsmType());
		$expected = 'qsdfghjklm';
		$this->_object->setOsmType($expected);
		$this->assertEquals($expected, $this->_object->getOsmType());
	}
	
	public function testGetPlaceId() : void
	{
		$this->assertEquals('azertyuiop', $this->_object->getPlaceId());
		$expected = 'qsdfghjklm';
		$this->_object->setPlaceId($expected);
		$this->assertEquals($expected, $this->_object->getPlaceId());
	}
	
	public function testGetPlaceRank() : void
	{
		$this->assertEquals('azertyuiop', $this->_object->getPlaceRank());
		$expected = 'qsdfghjklm';
		$this->_object->setPlaceRank($expected);
		$this->assertEquals($expected, $this->_object->getPlaceRank());
	}
	
	public function testGetSvg() : void
	{
		$this->assertEquals('azertyuiop', $this->_object->getSvg());
		$expected = 'qsdfghjklm';
		$this->_object->setSvg($expected);
		$this->assertEquals($expected, $this->_object->getSvg());
	}
	
	public function testGetCategory() : void
	{
		$this->assertEquals('azertyuiop', $this->_object->getCategory());
		$expected = 'qsdfghjklm';
		$this->_object->setCategory($expected);
		$this->assertEquals($expected, $this->_object->getCategory());
	}
	
	public function testGetType() : void
	{
		$this->assertEquals('azertyuiop', $this->_object->getType());
		$expected = 'qsdfghjklm';
		$this->_object->setType($expected);
		$this->assertEquals($expected, $this->_object->getType());
	}
	
	public function testGetAddresstype() : void
	{
		$this->assertNull($this->_object->getAddresstype());
		$expected = 'qsdfghjklm';
		$this->_object->setAddresstype($expected);
		$this->assertEquals($expected, $this->_object->getAddresstype());
	}
	
	public function testGetIcon() : void
	{
		$this->assertEquals('azertyuiop', $this->_object->getIcon());
		$expected = 'qsdfghjklm';
		$this->_object->setIcon($expected);
		$this->assertEquals($expected, $this->_object->getIcon());
	}
	
	public function testGetGeojson() : void
	{
		$this->assertNull($this->_object->getGeojson());
		$expected = $this->getMockBuilder(GeoJsonGeometryInterface::class)->disableOriginalConstructor()->getMock();
		$this->_object->setGeojson($expected);
		$this->assertEquals($expected, $this->_object->getGeojson());
	}
	
	public function testGetGeokml() : void
	{
		$this->assertNull($this->_object->getGeokml());
		$expected = 'qsdfghjklm';
		$this->_object->setGeokml($expected);
		$this->assertEquals($expected, $this->_object->getGeokml());
	}
	
	public function testGetGeotext() : void
	{
		$this->assertNull($this->_object->getGeotext());
		$expected = 'qsdfghjklm';
		$this->_object->setGeotext($expected);
		$this->assertEquals($expected, $this->_object->getGeotext());
	}
	
	public function testGetName() : void
	{
		$this->assertNull($this->_object->getName());
		$expected = 'qsdfghjklm';
		$this->_object->setName($expected);
		$this->assertEquals($expected, $this->_object->getName());
	}
	
	public function testGetNameDetails() : void
	{
		$this->assertNull($this->_object->getNameDetails());
		$expected = $this->getMockBuilder(ApiOrgOpenstreetmapNominatimNameDetails::class)->disableOriginalConstructor()->getMock();
		$this->_object->setNameDetails($expected);
		$this->assertEquals($expected, $this->_object->getNameDetails());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new ApiOrgOpenstreetmapNominatimGeocodingResult('azertyuiop', 'azertyuiop', 'azertyuiop', 'azertyuiop', 'azertyuiop', 'azertyuiop', 'azertyuiop', 'azertyuiop', 'azertyuiop', 'azertyuiop', 'azertyuiop', 'azertyuiop', 'azertyuiop', 'azertyuiop');
	}
	
}
