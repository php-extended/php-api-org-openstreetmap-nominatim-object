# php-extended/php-api-org-openstreetmap-nominatim-object

An implementation of the php-extended/php-api-org-openstreetmap-nominatim-interface library.

![coverage](https://gitlab.com/php-extended/php-api-org-openstreetmap-nominatim-object/badges/master/pipeline.svg?style=flat-square)
![build status](https://gitlab.com/php-extended/php-api-org-openstreetmap-nominatim-object/badges/master/coverage.svg?style=flat-square)


## Installation

The installation of this library is made via composer and the autoloading of
all classes of this library is made through their autoloader.

- Download `composer.phar` from [their website](https://getcomposer.org/download/).
- Then run the following command to install this library as dependency :
- `php composer.phar php-extended/php-api-org-openstreetmap-nominatim-object ^8`


## Basic Usage

This library may be used the following way :

```php

use PhpExtended\Osm\OsmNominatimApiRequest;
use PhpExtended\Osm\OsmNominatimApiEndpoint;

$api = new OsmNominatimApiEndpoint($client);

$request = new OsmNominatimApiRequest();
$request->setQuery('New York');

$response = $api->searchGeocode($request);

/* @var $response \PhpExtended\Osm\OsmNominatimApiGeocodingResult */
echo $response->getLatitude().' '.$response->getLongitude();

```


## License

MIT (See [license file](LICENSE)).
